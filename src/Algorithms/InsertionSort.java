package Algorithms;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

public class InsertionSort {
    public static int InsertionSort(int[] arr) {
        int statementCount = 0;

        int n = arr.length;
        for (int i = 1; i < n; i++) {
            int key = arr[i];
            int j = i - 1;

            statementCount++; // Increment for the comparison in the while loop

            while (j >= 0 && arr[j] > key) {
                statementCount += 2; // Increment for the comparison and assignment
                arr[j + 1] = arr[j];
                j--;
            }

            statementCount++; // Increment for the assignment outside the while loop
            arr[j + 1] = key;
        }

        return statementCount;
    }
    /**
    * Generates and returns an integer array of a specified size with random values within a given range.
    */
    public static int[] populateArray(int size, int minValue, int maxValue) {
        int[] arr = new int[size];
        Random random = new Random();

        for (int i = 0; i < size; i++) {
            arr[i] = random.nextInt(maxValue - minValue + 1) + minValue;
        }

        return arr;
    }
    /**
    * Loads integer data from a file and returns it as an integer array.
    */
    public static int[] loadData(String filename) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        String line = reader.readLine();
        String[] data = line.split(" ");
        int[] arr = new int[data.length];

        for (int i = 0; i < data.length; i++) {
            arr[i] = Integer.parseInt(data[i]);
        }

        reader.close();
        return arr;
    }
    /**
    * Demonstrates the Insertion Sort algorithm by sorting data from a file
    * and a randomly generated array and prints the results.
    */
    public static void main(String[] args) {
        try {

            // Read data from the input file
            int[] arrFromData = loadData("D:\\groupproject1\\inputAscendingInsertion.txt");

            // Generate a random array for sorting
            int[] arrRandom = populateArray(10000, 1, 10000); // Replace with your desired size and range

            // Sort data from the file and calculate the number of statements executed
            int statementsData = InsertionSort(arrFromData);
            int statementsRandom = InsertionSort(arrRandom);

            // Print the unsorted array from input file
            System.out.println("Unsorted array from inputAscendingInsertion.txt:");
            for (int num : arrFromData) {
                System.out.print(num + " ");
            }

            // Print the number of statements executed during sorting for input file data
            System.out.println("\nNumber of statements executed for inputAscendingInsertion.txt: " + statementsData);

            // Print the unsorted random array
            System.out.println("\nUnsorted random array:");
            for (int num : arrRandom) {
                System.out.print(num + " ");
            }

            // Print the unsorted random array and the number of statements executed
            System.out.println("\nNumber of statements executed for random array: " + statementsRandom);
        } catch (IOException e) {
            // Handle exceptions if there is an error reading the input file
            System.out.println("Error reading input file: " + e.getMessage());
        }
    }
}
