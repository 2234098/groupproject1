package Algorithms;

import java.io.*;

public class SelectionSort {
    public static void selectionSort(String[] arr) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            int minIndex = i;
            String minValue = arr[i];
            for (int j = i + 1; j < n; j++) {
                if (arr[j].compareTo(minValue) < 0) {
                    minIndex = j;
                    minValue = arr[j];
                }
            }
            if (minIndex != i) {
                // Swap arr[i] and arr[minIndex]
                String temp = arr[i];
                arr[i] = arr[minIndex];
                arr[minIndex] = temp;
            }
        }
    }

    public static void main(String[] args) {
        try {
            // Read data from the input file
            BufferedReader reader = new BufferedReader(new FileReader("inputSelection.txt"));
            String line;
            StringBuilder content = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                content.append(line).append("\n");
            }
            reader.close();

            // Split the content into an array of strings
            String[] data = content.toString().split("\n");

            // Apply selection sort
            selectionSort(data);

            // Write sorted data to the output file
            BufferedWriter writer = new BufferedWriter(new FileWriter("outputSelection.txt"));
            for (String str : data) {
                writer.write(str);
                writer.newLine();
            }
            writer.close();

            System.out.println("Sorting and writing to file completed.");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
