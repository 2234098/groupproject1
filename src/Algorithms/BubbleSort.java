package Algorithms;

import java.io.*;

public class BubbleSort {
    public static void bubbleSort(String[] arr) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (arr[j].compareTo(arr[j + 1]) > 0) {
                    // Swap arr[j] and arr[j+1]
                    String temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }

    public static void main(String[] args) {
        try {
            // Read data from the input file
            BufferedReader reader = new BufferedReader(new FileReader("inputBubble.txt"));
            String line;
            StringBuilder content = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                content.append(line).append("\n");
            }
            reader.close();

            // Split the content into an array of strings
            String[] data = content.toString().split("\n");

            // Apply bubble sort
            bubbleSort(data);

            // Write sorted data to the output file
            BufferedWriter writer = new BufferedWriter(new FileWriter("outputBubble.txt"));
            for (String str : data) {
                writer.write(str);
                writer.newLine();
            }
            writer.close();

            System.out.println("Sorting and writing to file completed.");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
