/**
 * Authors: Group A
 * Date: 07/11/2023
 * Group Project 2: Huffman coding
 */

package IT212_9443_MidGroupA.midlab2;

public class TreeNode <T> implements Comparable<TreeNode<T>>{
    //Variable declaration
    private TreeNode<T> leftNode;
    private T data;
    private int freq;
    private TreeNode<T> rightNode;
    private int bits;
    private String huffmanCode;

    /**
     * Constructor class
     * Creates a TreeNode with the provided data, initializing left and right nodes to null.
     * @param nodeData
     */
    public TreeNode(T nodeData){
        this.data = nodeData;
        this.leftNode = this.rightNode = null;
    }// end of TreeNode

    /**
     * Constructor class
     * Creates a TreeNode with the provided data and frequency.
     * @param nodeData
     * @param freq
     */
    public TreeNode(T nodeData, int freq){
        this.data = nodeData;
        this.freq = freq;
    }

    /**
     * Returns the left child node of the current node.
     * @return leftNode of current node
     */
    public TreeNode<T> getLeftNode(){
        return leftNode;
    }

    /**
     * Lets the left node of the current node to the node passed in as a parameter
     * @param leftNode
     */
    public void setLeftNode(TreeNode<T> leftNode){
        this.leftNode = leftNode;
    }

    /**
     * Returns right node of the current node
     * @return right node
     */
    public TreeNode<T> getRightNode(){
        return rightNode;
    }

    /**
     * Sets the right child node of the current node to the specified node.
     * @param rightNode
     */
    public void setRightNode(TreeNode<T> rightNode){
        this.rightNode = rightNode;
    }

    /**
     * Returns data stored in node
     * @return data of the node
     */
    public T getData(){
        return data;
    }

    /**
     * Sets the data stored in the node.
     * @param data
     */
    public void setData(T data){
        this.data = data;
    }

    /**
     * Returns data of the node
     * @return String value of data
     */
    @Override
    public String toString(){
        return data.toString();
    }

    /**
     * Inserts a given value to the node
     * @param insertValue
     */
    public void insert(TreeNode<T> insertValue){
        if (leftNode == null){
            this.leftNode = insertValue;
        } else if (rightNode == null) {
            this.rightNode = insertValue;
        }
    }

    /**
     * Returns the frequency associated with the node (used in Huffman coding).
     * @return
     */
    public int getFreq(){
        return freq;
    }

    /**
     * Sets the frequency associated with the node (used in Huffman coding).
     * @param freq
     */
    public void setFreq(int freq){
        this.freq = freq;
    }

    /**
     * Compares frequency of node
     * @param node the object to be compared.
     * @return 0 if frequency of node is less than the other node
     *         1 if greater,
     *         -1 otherwise (if they are equal)
     */
    @Override
    public int compareTo(TreeNode other) {
        if(this.getFreq()==other.getFreq()){
            return 0;
        }else if(this.getFreq()< other.getFreq()) {
            return -1;
        }else {
            return 1;
        }
    }

    /**
     * Returns number of bits
     * @return number of bits
     */
    public int getBits(){
        return bits;
    }

    //Sets the number of bits in the bitmap
    public void setBits(int bits){
        this.bits = bits;
    }

    /**
     * Returns the Huffman code for a given character
     * @return Huffman code
     */
    public String getHuffmanCode(){
        return huffmanCode;
    }

    /**
     * Sets the Huffman code for a given character
     * @param huffmanCode
     */
    public void setHuffmanCode(String huffmanCode){
        this.huffmanCode = huffmanCode;
    }

    @Override
    public boolean equals(Object obj) {
        TreeNode node = (TreeNode) obj;
        if(this.data.equals(node.data)){
            return true;
        }else{
            return false;
        }
    }
}
