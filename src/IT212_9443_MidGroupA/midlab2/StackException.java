/**
 * Authors: Group A
 * Date: 07/11/2023
 * Group Project 2: Huffman coding
 */

package IT212_9443_MidGroupA.midlab2;

public class StackException extends RuntimeException{
    // Calls constructor of class
    /**
     * @param message The error message describing the exception.
     */
    public StackException(String message){
        super(message);
    }
} // end of class StackUnderflowException
