/**
 * Authors: Group A
 * Date: 07/11/2023
 * Group Project 2: Huffman coding
 */

package IT212_9443_MidGroupA.midlab2;
import java.util.LinkedList;

public class Queue<T>{
    // Declare variables
    private LinkedList<T> list;

    // Constructor that is called when an object is created
    public Queue(){
        list = new LinkedList<>();
    }

    /**
     * Returns a reference to the list
     * @return list
     */
    public LinkedList<T> getList() {
        return list;
    }

    /**
     * Sets list
     * @param list
     */
    public void setList(LinkedList<T> list) {
        this.list = list;
    }

    /**
     * Clears the list
     */
    public void clear(){
        list.clear();
    }

    /**
     * Returns ture if list is empty, flase if otherwise
     * @return isEmpty
     */
    public boolean isEmpty(){
        return list.isEmpty();
    }

    /**
     * Gets first element of the list
     * @return first element of list
     */
    public T firstElement(){
        return list.getFirst();
    }

    /**
     * Returns and deletes first element
     * @return first element
     */
    public T dequeue(){
        return list.removeFirst();
    }

    /**
     * Adds element at the start of the list
     * @param element
     */
    public void enqueue(T element){
        list.add(element);
    }

    /**
     * Returns a string representation of the object
     * @return string of the object
     */
    @Override
    public String toString() {
        return list.toString();
    }
}
