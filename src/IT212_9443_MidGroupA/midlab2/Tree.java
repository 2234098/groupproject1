/**
 * Authors: Group A
 * Date: 07/11/2023
 * Group Project 2: Huffman coding
 */

package IT212_9443_MidGroupA.midlab2;

public class Tree<T>  {
    // Variable declaration
    private TreeNode<T> root;

    /**
     * Constructor for Tree class.
     * Initializes the root to null.
     */
    public Tree(){
        root = null;
    }

    /**
     * Sets the root of the tree to the provided node
     * @param root, The node to set as the root of the tree
     */
    public void setRoot(TreeNode<T> root){
        this.root = root;
    }

    /**
     * Retrieves the root of the tree
     * @return root node of the tree
     */
    public TreeNode<Character> getRoot(){
        return (TreeNode) root;
    }

    /**
     * Inserts a new node with the provided data into the tree.
     * If the tree is empty, the new node becomes the root.
     * @param insertInfo
     */
    public void insertNode(T insertInfo){
        if (root == null){
            root = (TreeNode<T>) insertInfo;
            return;
        }
        root.insert((TreeNode<T>) insertInfo);
    }

    /**
     * Performs a pre-order traversal of the tree starting from the given node.
     * This means it prints the current node, then the left node, and finally the right node.
     * @param node
     */
    public void preOrderTraversal(TreeNode<Character> node){
        if (node == null){
            return;
        }
        System.out.println(node.getData());
        preOrderTraversal(node.getLeftNode());
        preOrderTraversal(node.getRightNode());
    }

    /**
     * Performs an in-order traversal of the tree starting from the given node.
     * This means it first traverses the left node, then prints the current node,
     * and finally traverses the right node.
     * @param node, The node to start the traversal from.
     */
    public void inOrderTraversal(TreeNode<Character> node){
        if (node == null){
            return;
        }
        inOrderTraversal(node.getLeftNode());
        System.out.println(node.getData() + " ");
        inOrderTraversal(node.getRightNode());
    }

    /**
     * Performs a post-order traversal of the tree starting from the given node.
     * This means it first traverses the left node, then the right node, and finally prints the current node.
     * @param node
     */
    public void postOrderTraversal(TreeNode<Character> node){
        if (node == null){
            return;
        }
        postOrderTraversal(node.getLeftNode());
        postOrderTraversal(node.getRightNode());
        System.out.println(" " + node.getData());
    }
} // End of Tree class
