/**
 * Authors: Group A
 * Date: 07/11/2023
 * Group Project 2: Huffman coding
 *
 * Algorithm:
 *  1. Variable Declaration: At the beginning of the program, several variables are declared. These include givenHuffmanCode and givenTextcode as String variables.
 *
 *  2. freqCounter: This method counts the frequency of each character in the input text and creates a priority queue (pq) and a regular queue (queue) to store the characters and their frequencies in priority order.
 *
 *  3. bitRepresentations: This recursive method is used to assign Huffman codes to each character in a Huffman tree. It traverses the Huffman tree and assigns '0' and '1' bits to each character accordingly.
 *
 *  4. createHuffmanTree: This method creates a Huffman tree from the characters and their frequencies stored in the priority queue. It repeatedly removes the two nodes with the lowest frequencies from the queue, combines them into a new node, and adds it back to the queue. This process continues until there is only one node left in the queue, which is the root of the Huffman tree.
 *
 *  5. printTable: This method prints a table of values for the characters in the text, including their Huffman codes and the number of bits required to represent them in Huffman coding. It also calculates the percentage of storage savings achieved by Huffman coding compared to using ASCII.
 *
 *  6. decodeHuffmanCode: This method takes a Huffman code as input and decodes it back to the original text using the Huffman tree. It traverses the tree based on '0' and '1' bits, and when it reaches a leaf node, it appends the corresponding character to the decoded text.
 *
 *  7. textToHuffmanCode: This method converts a given text into its corresponding Huffman code using the Huffman tree. It iterates through the characters in the input text and calls the findHuffmanCode method to find the Huffman code for each character.
 *
 *  8. findHuffmanCode: This is a recursive method that searches for the Huffman code of a specific character in the Huffman tree. It traverses the tree by recursively exploring the left and right branches until it finds the character.
 *
 *  9. displayTextToHuffmanCode: This method displays the Huffman code for a given input text using a graphical user interface (GUI). It calls the textToHuffmanCode method to obtain the Huffman code and displays it in a dialog box.
 *
 *  10. Main Method: The main method appears to be missing in the provided code. It's typically the entry point of a Java program, and it's where you would call the methods to execute the Huffman coding process.
 *
 */


package IT212_9443_MidGroupA.midlab2;
import javax.swing.*;
import java.awt.*;
import java.util.Queue;
import java.util.PriorityQueue;

public class Huffman {
    // variable declaration
    public static String givenHuffmanCode = "";
    public static String givenTextcode = "";

    /**
     * Function that counts the number of occurrences of
     * @param text - text to be encoded in Huffman
     */
    public static void freqCounter(String text, PriorityQueue<TreeNode<Character>> pq, java.util.Queue<TreeNode<Character>> queue) {
        TreeNode<Character> node;
        char letter;
        int numberOfCharacters = 0;
        int count = 0;
        String [] letters = text.split("(?!^)"); // Splits given string into a String array

        while (numberOfCharacters != text.length()) {
            for (int i = 32; i < 123; i++) {
                letter = (char) i;
                if (i == 33) { // Check if value of i is equal to 33, to avoid other characters from the ascii table,
                    i = 64; // skip to 65, to start at letter A.
                    continue;
                }

                if (i == 91) { // Check if value of i is equal to 91, in ASCII table, 91 - 96 corresponds to symbols,
                    i = 96;     // Hence, make i to 96 to skip these symbol values
                    continue;
                }
                else if (numberOfCharacters == text.length()) // check if number of characters is already equal to
                    // number of characters of given string, if so
                    break;                                       // break

                for (int j = 0; j < text.length(); j++) {
                    if (letter == letters[j].charAt(0)) { // check if letter is equal to index j of letters array
                        numberOfCharacters += 1; // increment total number of characters
                        count++; // temporary counter
                    }
                } // end of inner for loop


                if (count != 0) { // if count is not 0, means that a character has a corresponding frequency value,
                    // therefore, create a node for the character with frequency

                    node = new TreeNode<>(letter, count);
                    pq.add(node); // add new node to priority queue
                    queue.add(node);
                } // end of if
                count = 0;
            } // end of outer loop
        } // end of while loop
    } // end of freqCounter

    /**
     * Iterative function to encode huffman code derived from huffman tree and set number of bits per character node
     * @param root root of the huffman tree
     * @param s String that will hold huffman code of each character
     */
    public static void bitRepresentations(TreeNode<Character> root, String s) {
        try {
            // print tree
            if (root.getLeftNode() == null && root.getRightNode() == null && (Character.isLetter(root.getData()) || root.getData() == ' ')){
            /*
                Set bits for each node and assign the encoded huffman code to corresponding character node
             */
                root.setBits(s.length());
                root.setHuffmanCode(s);
                return;
            }
            bitRepresentations(root.getLeftNode(), s + "0");
            bitRepresentations(root.getRightNode(), s + "1");
        } catch (NullPointerException nullPointerException) {
            JOptionPane.showMessageDialog(null, "PLEASE ENTER AT LEAST 2 CHARACTERS.");
        }
    } // end of bitRepresentations

    /**
     * Function that creates a huffman tree given
     * @param cTree base tree for huffman
     * @param q queue that holds the nodes of each character to be placed in the tree
     */
    public static void createHuffmanTree (Tree<TreeNode<Character>> cTree, PriorityQueue<TreeNode<Character>> q) {
        for (int i = q.size(); i > 1; i-- ) {

            /*
                Pop two nodes with the least frequency
             */
            TreeNode<Character> minNode1 = q.peek();
            q.poll();
            TreeNode<Character> minNode2 = q.peek();
            q.poll();

            /*
                Create a new node/root for subtrees that has a frequency of the sum of two minimum nodes
             */
            Tree<TreeNode<Character>> sum = new Tree<>();
            sum.insertNode(new TreeNode<>('-', minNode1.getFreq() + minNode2.getFreq()));

            /*
                Check if both minimum nodes are equal
                Set left node of root with the node that has a corresponding data
                else, insert the node
             */
            if (minNode1.getFreq() == minNode2.getFreq()) {
                if (Character.isLetter(minNode1.getData())) {
                    sum.getRoot().setLeftNode(minNode1);
                    sum.getRoot().setRightNode(minNode2);
                }
                else {
                    sum.getRoot().setLeftNode(minNode2);
                    sum.getRoot().setRightNode(minNode1);
                }
            }
            else { // insert nodes
                sum.insertNode(minNode1);
                sum.insertNode(minNode2);
            }

            cTree = sum; // set tree

            q.add(sum.getRoot()); // add new node to queue
        } // end of for loop
        bitRepresentations(cTree.getRoot(), ""); // encode huffman code
    } // end of createHuffmanTree method

    /**
     * function that prints the table of values for text to huffman as well as the percentage of storage savings
     * @param text given text to be decoded
     * @param treeNodeQueue queue of characters where character nodes are stored
     */
    public static void printTable(String text, java.util.Queue<TreeNode<Character>> treeNodeQueue) {
        int asciiBitsNeeded = text.length() * 7; // using ASCII as basis for each character
        int bitsNeeded = 0;
        double percentageOSS;

        StringBuilder huffmanCode = new StringBuilder();
        StringBuilder output = new StringBuilder();

        /*
            Print header
         */
        output.append(String.format("%-15s%-10s%-20s%-10s%-5s%10s%n", "Character", "|", "Huffman Code", "|", "# of Bits", " "));
        while (!treeNodeQueue.isEmpty()) { // while queue is not empty
            TreeNode<Character> temp = treeNodeQueue.poll();
            bitsNeeded += temp.getFreq() * temp.getBits(); // compute bits needed
            huffmanCode.append(temp.getHuffmanCode()).append(" "); // construct final encoded huffman code
            output.append(String.format("%-15s%-10s%-20s%-10s%-5s%10s%n",(temp.getData() == ' ' ? "[Space]" : temp.getData()) , "|", temp.getHuffmanCode(), "|", temp.getBits(), " "));
        } // end of while

        percentageOSS = (((double) (asciiBitsNeeded - bitsNeeded)) / (asciiBitsNeeded)) * 100; // compute % of storage savings
        output.append(String.format("%n%s%-15s%10s"," ", "Huffman Code: ", huffmanCode ));
        output.append(String.format("%n%n%-15s%-15s%.2f%s", " ", "Percentage of Storage Savings: " , percentageOSS, "%"));
        output.append(String.format("%n%n%-10s", "DECODE A HUFFMAN CODE"));


        //display the table of values
        JTextArea textArea = new JTextArea(output.toString());
        textArea.setFocusable(true);
        textArea.setFont(new Font(Font.MONOSPACED, Font.BOLD, 14));
        textArea.setBorder(null);
        textArea.setBackground(null);

        // do while loop that asks the user to input a huffman code based on the provided table
        try {
            do {
                givenHuffmanCode = JOptionPane.showInputDialog(null, textArea, "Input a huffman code " +
                        "based from the table of values.\n NOTE: Do not include spaces");
                if (givenHuffmanCode == null) {
                    new StringBuilder(String.valueOf(output));
                    continue;
                } // end of if
                if (!givenHuffmanCode.matches("[0-9]+")) {
                    JOptionPane.showMessageDialog(null, "Please input valid Huffman Code only.");
                    givenHuffmanCode = "";
                    continue;
                }
                if (givenHuffmanCode.equals("")) {
                    //Displays an error message when there is no input
                    JOptionPane.showMessageDialog(null, """
                                    Please input a Huffman Code""",
                            "INPUT ERROR", JOptionPane.ERROR_MESSAGE);
                }   // end of if
            } while (givenHuffmanCode.equals("") || !givenHuffmanCode.matches("[0-9]+"));

        } catch (NullPointerException nullPointerException) {
            JOptionPane.showMessageDialog(null, "Decoding Cancelled.");
        }
        new StringBuilder(String.valueOf(output));

    } // end of printTable

    /**
     * Decodes a given huffman code into a String text
     * @param code - huffman code to be decoded
     * @param priorityQueue - queue that holds the root of the huffman tree
     */
    public static void decodeHuffmanCode(String code, PriorityQueue<TreeNode<Character>> priorityQueue) {
        StringBuilder decode = new StringBuilder();
        StringBuilder output = new StringBuilder();
        TreeNode<Character> tn = priorityQueue.peek(); // assign

        try {
            /*
                Traverse the tree
             */
            for (int i = 0; i < code.length(); i++) {
                char c = code.charAt(i);
                if (c == '0')
                    tn = tn.getLeftNode();
                else
                    tn = tn.getRightNode();

        /*
            If last node, append data of last node to the StringBuilder and set temporary node back to root
         */
                if (tn.getLeftNode() == null) {
                    decode.append(tn.getData());
                    tn = priorityQueue.peek();
                }
            } // end of for loop

            // the value of the decoded Huffman Code
            if (code.equals("Input a huffman code based from the table of values. NOTE: Do not include spaces"))
                code = " ";
            output.append(String.format("%1s%-15s%-10s", " ", "\nHuffman Code: ", code));
            output.append(String.format("%1s%-15s%-15s", " ", "\nDecoded Text: ", decode));
        } catch (NullPointerException nullPointerException) {
            JOptionPane.showMessageDialog(null, "No Huffman Code Input Found."," ", JOptionPane.ERROR_MESSAGE);
        }

        //display the table of values
        JTextArea textArea = new JTextArea(output.toString());
        textArea.setFocusable(true);
        textArea.setFont(new Font(Font.MONOSPACED, Font.BOLD, 14));
        textArea.setBorder(null);
        textArea.setBackground(null);
        if (!output.toString().isEmpty())
            JOptionPane.showMessageDialog(null,textArea);
    } // end of decodeHuffmanTree

    /**
     * Converts text to Huffman code based on the Huffman tree
     *
     * @param textInput      - Text to be encoded into Huffman code
     * @param priorityQueue  - Queue that holds the root of the Huffman tree
     * @return Encoded Huffman code
     */
    public static String textToHuffmanCode(String textInput, PriorityQueue<TreeNode<Character>> priorityQueue) {
        TreeNode<Character> tn = priorityQueue.peek();
        StringBuilder huffmanCode = new StringBuilder();
        StringBuilder output = new StringBuilder();

        for (int i = 0; i < textInput.length(); i++) {
            char character = textInput.charAt(i);
            String code = findHuffmanCode(character, tn);
            if (code != null) {
                huffmanCode.append(code);
            } else {
                JOptionPane.showMessageDialog(null, "Character '" + character + "' does not have a Huffman code.", "Error", JOptionPane.ERROR_MESSAGE);
                return null;
            }
        }
        return huffmanCode.toString();
    }

    /**
     * Find the Huffman code for a specific character in the Huffman tree
     *
     * @param character   - The character to find the Huffman code for
     * @param root        - The root of the Huffman tree
     * @return The Huffman code for the character
     */
    public static String findHuffmanCode(char character, TreeNode<Character> root) {
        if (root == null) {
            return null;
        }

        if (root.getLeftNode() == null && root.getRightNode() == null && root.getData() == character) {
            return "";
        }

        String leftCode = findHuffmanCode(character, root.getLeftNode());
        if (leftCode != null) {
            return "0" + leftCode;
        }

        String rightCode = findHuffmanCode(character, root.getRightNode());
        if (rightCode != null) {
            return "1" + rightCode;
        }

        return null;
    }

    /**
     * Display the Huffman code for a given text
     *
     * @param textInput     - Text to be encoded into Huffman code
     * @param priorityQueue - Queue that holds the root of the Huffman tree
     */
    public static void displayTextToHuffmanCode(String textInput, PriorityQueue<TreeNode<Character>> priorityQueue) {
        String huffmanCode = textToHuffmanCode(textInput, priorityQueue);

        if (huffmanCode != null) {
            StringBuilder output = new StringBuilder();
            output.append(String.format("%n%s%-15s%10s", " ", "Huffman Code: ", huffmanCode));

            // Display the Huffman code for the given text
            JTextArea textArea = new JTextArea(output.toString());
            textArea.setFocusable(true);
            textArea.setFont(new Font(Font.MONOSPACED, Font.BOLD, 14));
            textArea.setBorder(null);
            textArea.setBackground(null);

            JOptionPane.showMessageDialog(null, textArea);
        } else {
            // Handle the case where there was an issue calculating the Huffman code
            // You can display an error message or handle it according to your needs.
        }
    }

} // end of Huffman