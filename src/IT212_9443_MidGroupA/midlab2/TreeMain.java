/**
 * Authors: Group A
 * Date: 07/11/2023
 * Group Project 2: Huffman coding
 *
 * Algorithm:
 *  Main Program:
 * 1. The main method is the entry point of the program and creates an instance of the TreeMain class to start the program.
 *
 *  TreeMain Class:
 * 2. The program starts with the run method, which displays the main menu using a graphical dialog box with options to create Huffman codes or exit.
 *     -> When the "Create Huffman Code" option is chosen, it calls the createHuffmanMenu method.
 *
 *  createHuffmanMenu Method:
 * 3. This method first displays a submenu to choose the input type: either entering text directly or using a text file.
 *      -> If the user selects to input text, it prompts the user to enter the text.
 *      -> If the user selects to input a text file, it opens a file dialog using JFileChooser and ensures that a valid .txt file is selected.
 *      -> After obtaining the input text, it checks if there are at least 2 unique characters in the text using the inputCheck method.
 *      -> It then proceeds to create a Huffman tree and generate a table of values for text-to-Huffman code conversion.
 *      -> The printTable method displays the table using a GUI dialog.
 *      -> It prompts the user to input a Huffman code and calls the decodeHuffmanCode method to decode the input code.
 *      -> It then allows the user to enter text, which is converted to Huffman code using the displayTextToHuffmanCode method.
 *      -> The program provides visual feedback using GUI dialogs throughout these steps.
 *
 *  getFilePath Method:
 * 4. This method opens a file dialog using JFileChooser and returns the path of the selected file, ensuring it's a valid .txt file.
 *
 *  Exit Method:
 * 5. The Exit method displays a farewell message to the user and exits the program when the user chooses the "Exit" option.
 *
 *  Utility Methods:
 * 6. The inputCheck method checks if a given text contains at least 2 unique characters to ensure valid input.
 *      -> The subMenu method displays a submenu to choose the input type (text or file) and handles the corresponding user inputs.
 *      -> The program uses JOptionPane dialogs for input, messages, and displays. It also makes use of a GUI with icons for a user-friendly interface.
 */

package IT212_9443_MidGroupA.midlab2;
// imports
import IT212_9443_MidGroupA.midlab2.StackException;
import java.util.LinkedList;
import javax.swing.*;
import java.awt.*;
import java.util.Queue;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Scanner;
import javax.swing.JFileChooser;

import static IT212_9443_MidGroupA.midlab2.Huffman.givenHuffmanCode;
import static IT212_9443_MidGroupA.midlab2.Huffman.givenTextcode;

public class TreeMain {
    // images used in the program
    static ImageIcon InputIcon = new ImageIcon(new ImageIcon("src/Midterms.icons/InputIconNumberTwo.png").getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH));
    static ImageIcon menuIcon = new ImageIcon(new ImageIcon("src/Midterms.icons/MainMenu.png").getImage().getScaledInstance(120, 120, Image.SCALE_SMOOTH));
    static ImageIcon goodbyeIcon = new ImageIcon(new ImageIcon("src/Midterms.icons/Exit.png").getImage().getScaledInstance(100, 100, Image.SCALE_SMOOTH));
    static Scanner kbd = new Scanner(System.in); // A static variable that is used to read input from the keyboard
    public String givenText;

    /**
     * Main method that runs the run() method
     */
    public static void main(String[] args) {
        // Creating a new object of the MainMenu class.
        TreeMain menu= new TreeMain();

        // run the run() method
        try {
            menu.run();
        } catch (NumberFormatException exception) {
            exception.printStackTrace();
        } catch (StackException | IOException e) {
            throw new RuntimeException(e);
        }
        // exit the program
        System.exit(0);
    } // end of main method

    /**
     * Show the menu of the program
     */
    public void run() throws IOException {
        int choice;
        // options in the program
        String[] optionsMainMenu = {"Create Huffman Code\n", "Exit"};
        // do while that creates choices for the program
        do {
            choice = JOptionPane.showOptionDialog(null, "       Midterm Project 2 \n" +
                            "       HUFFMAN CODE", "MAIN MENU",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, menuIcon, optionsMainMenu, null);
            switch (choice) {
                case 0 -> createHuffmanMenu();
                case 1 -> Exit();
            } //end of switch case
        } while (choice != 1); //end of do-while loop
    } // end of run method

    /**
     * Sub menu for input type
     */
    public void subMenu() throws IOException {
        int choice;
        // Sub options
        String [] subOptions = {"Text", "Text File", "Back to Main Menu"};
        do {
            choice = JOptionPane.showOptionDialog(null, "Choose Input Type \n","Input Type Menu",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, menuIcon, subOptions, null);
            switch (choice) {
                case 0 -> { // ask the user to input a text
                    do {
                        String text = JOptionPane.showInputDialog(null, "Enter Text");
                        //if user clicks cancel, return to menu
                        if(text == null){
                            subMenu();
                        }
                        this.givenText = text;
                        inputCheck(choice);
                    } while (givenText.equals(""));
                    return;
                }
                case 1 -> { // ask the user to input file path
                    String filePath = getFilePath();
                    if (filePath == null){
                        return;
                    }
                    this.givenText = filePath;
                    inputCheck(choice);
                    return;
                }

                case 2 -> run();
            }
        } while (choice != 2 ); // while choice is not 3 or "Back to Main Menu" option


    } // end of subMenu method


    /**
     * this method gets the file path of the file to be used in the
     * program, it makes use of the JFileChooser class to open
     * a window in which the user can select a file
     * @return fileName - path of the file to be used
     */
    public String getFilePath() throws IOException {
        String filePath;
        while(true){
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
            int choice = fileChooser.showOpenDialog(null);

            if (choice == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                filePath = String.valueOf(file);
                //Checks if the file is a text file
                if (fileChooser.getTypeDescription(file).equals("txt")) {
                    return filePath;
                }
                //display an error message if the file is not a .txt file
                else{
                    JOptionPane.showMessageDialog(null, """
                            Please select a .txt file
                            """,
                            "ERROR!", JOptionPane.ERROR_MESSAGE);
                }
            }
            else{
                //returns to the menu if user clicks "cancel"
                subMenu();
            }
        }
    }

    /**
     * Checks input if text or a text file to be converted in huffman code
     * @param choice choice
     * @throws IOException throws an exception when file is not found
     */
    public void inputCheck(int choice) throws IOException {
        boolean check = true;
        do {
            if (givenText == null) {
                return;
            } // end of if
            else {
                Path fileName;
                String text;
                switch (choice) {
                    case 0 -> {
                        check = !inputCheck(givenText);
                        if (givenText.equals("")) {
                            //Displays an error message when there are no input
                            JOptionPane.showMessageDialog(null, """
                                                Please input a text""",
                                    "INPUT ERROR", JOptionPane.ERROR_MESSAGE);
                            return;
                        } // end of if
                        else if (!check) {
                            JOptionPane.showMessageDialog(null, "PLEASE ENTER AT LEAST 2 UNIQUE CHARACTERS.",
                                    "Invalid Input.", JOptionPane.ERROR_MESSAGE);
                            return;
                        }
                    }
                    case 1 -> {
                        if (!givenText.isEmpty()) {
                            fileName = Path.of(givenText);
                        }
                        else {
                            JOptionPane.showMessageDialog(null, "\t\t\tNo Path Input \nA default table of values" +
                                    " will be generated");
                            fileName = Path.of("src/Midterms.midlab2/sample.txt");
                        }
                        text = Files.readString(fileName);
                        givenText = text;
                    }
                }
            } // end of else
            // end of do-while loop
        } while (givenText.equals(""));
    }

    /**
     * Creates the huffman tree and provide a table that will be the basis for
     * the user what huffman code to use
     */
    public void createHuffmanMenu() throws IOException {
        // scans the input of the user
        kbd = new Scanner(System.in);

        subMenu();

        Queue<TreeNode<Character>> q = new LinkedList<>();

        PriorityQueue<TreeNode<Character>> treeNodeQueue = new PriorityQueue<>(this.givenText.length(), new Comparator<>() {
            /**
             * Function that compares the frequency of given node 1 to the frequency of each node in the queue
             *
             * @param node1 the first object to be compared.
             * @param node2 the second object to be compared.
             */
            @Override
            public int compare(TreeNode<Character> node1, TreeNode<Character> node2) {
                return node1.getFreq() - node2.getFreq();
            }
        });

        Tree<TreeNode<Character>> charTree = new Tree<>();      // Tree that will hold the huffman tree
        Huffman.freqCounter(this.givenText, treeNodeQueue, q);  // run the freqCounter method
        Huffman.createHuffmanTree(charTree, treeNodeQueue);     // run the createHuffmanTree method


        // shows an instruction message
        JOptionPane.showMessageDialog(null, """
                        INPUT A HUFFMAN CODE
                        BASED ON THE HUFFMAN
                        CODE PROVIDED IN THE
                        TABLE""", "Decode the Huffman Code",
                JOptionPane.INFORMATION_MESSAGE, InputIcon);

        Huffman.printTable(this.givenText, q);     // run the printTable method
        if (!(givenHuffmanCode == null)) {
            Huffman.decodeHuffmanCode(givenHuffmanCode, treeNodeQueue); // run the decodeHuffmanCode method
            do {
                givenTextcode = JOptionPane.showInputDialog(null, "Enter Text to convert to huffman code");
                if (givenTextcode == null) {
                    break;
                }
                if (givenTextcode.equals("")) {
                    JOptionPane.showMessageDialog(null, "Please input a text", "INPUT ERROR", JOptionPane.ERROR_MESSAGE);
                } else if (!(givenTextcode == null)) {
                    Huffman.displayTextToHuffmanCode(givenTextcode, treeNodeQueue); // run the textToHuffmanCode method
                }
            } while (givenTextcode.equals(""));
        }   // end of createHuffmanMenu method
    }

    /**
     * Checks a given text if it contains at least 2 characters
     * @param text input text
     * @return true if text only consists of one character, otherwise false
     */
    public static boolean inputCheck(String text ) {
        int length = text.length();
        for (int i = 1; i < length; i++)
            if  (text.charAt(i) != text.charAt(0))
                return false;

        return true;
    } // end of inputCheck

    /**
     * Prompts a message and exits the program
     */
    public void Exit() {
        JOptionPane.showMessageDialog(null, "Thank you for using\nour program.","EXIT", JOptionPane.INFORMATION_MESSAGE, goodbyeIcon);
        System.exit(0);
    }   // end of Exit method
}   // end of TreeMain
