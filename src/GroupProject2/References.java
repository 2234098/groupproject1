/**
 * Author : GroupFrostBourne
 * Title : PrelimGroupProject2
 *
 * Process:
 *  1. Class Definition (References):
 *      The code defines a class named References.
 *
 *  2. Private Member Variable (Gmenu):
 *      The class has a private member variable Gmenu of type GClassMenu. This variable is used to store an instance of the GClassMenu class.
 *
 *  3. Constructor (References()):
 *      The class has a constructor that initializes the Gmenu member variable by creating a new instance of GClassMenu.
 *
 *  4. Getter and Setter Methods:
 *      The class defines a series of getter and setter methods for different elements of the GClassMenu object (Create, Read, Update, Delete, Search, Createclass, DeleteClass, Updateclass).
 *      Each getter method returns the corresponding element from the Gmenu object.
 *      Each setter method sets the corresponding element in the Gmenu object to the provided value.
 *      For example, getCreate() returns the Create element from Gmenu, and setCreate(JMenu create) sets the Create element in Gmenu to the provided create value.
 *
 *  5. Getter and Setter Methods for Various Elements:
 *      There are multiple pairs of getter and setter methods, each corresponding to a different element within the GClassMenu object. This allows external code to get and set values for specific elements within Gmenu.
 *
 *  6. Usage of the GClassMenu Object (Gmenu):
 *      The References class effectively encapsulates a GClassMenu object and provides controlled access to its elements through getter and setter methods.
 *
 *  7. GClassMenu Class:
 *      It's important to note that the GClassMenu class is assumed to be defined elsewhere in the codebase. The References class is designed to work with this class, providing access to its elements.
 *      In summary, the References class serves as a wrapper or intermediary for a GClassMenu object, offering getter and setter methods to access and modify specific elements within that object. This design promotes encapsulation and controlled access to the underlying GClassMenu elements.
 *
 */

package GroupProject2;

import javax.swing.*;

public class References {

    private GClassMenu Gmenu;

    public References() {
        Gmenu = new GClassMenu();
    }

    public JMenu getCreate() {
        return Gmenu.Create;
    }

    public void setCreate(JMenu create) {
        Gmenu.Create = create;
    }

    public JMenu getRead() {
        return Gmenu.Read;
    }

    public void setRead(JMenu read) {
        Gmenu.Read = read;
    }

    public JMenu getUpdate() {
        return Gmenu.Update;
    }

    public void setUpdate(JMenu update) {
        Gmenu.Update = update;
    }

    public JMenu getDelete() {
        return Gmenu.Delete;
    }

    public void setDelete(JMenu delete) {
        Gmenu.Delete = delete;
    }

    public JMenu getSearch() {
        return Gmenu.Search;
    }

    public void setSearch(JMenu search) {
        Gmenu.Search = search;
    }

    public JMenuItem getCreateclass() {
        return Gmenu.Createclass;
    }

    public void setCreateclass(JMenuItem createclass) {
        Gmenu.Createclass = createclass;
    }

    public JMenuItem getDeleteClass() {
        return Gmenu.DeleteClass;
    }

    public void setDeleteClass(JMenuItem deleteClass) {
        Gmenu.DeleteClass = deleteClass;
    }

    public JMenuItem getUpdateclass() {
        return Gmenu.Updateclass;
    }

    public void setUpdateclass(JMenuItem updateclass) {
        Gmenu.Updateclass = updateclass;
    }
}