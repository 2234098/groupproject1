/**
 * Author : GroupFrostBourne
 * Title : PrelimGroupProject2
 *
 * Process :
 *  1. Class Definition:
 *      The code defines a generic class LinkedList<E> for implementing a singly linked list. It uses generics to allow the list to hold elements of any data type.
 *
 *  2. Inner Node Class (Node<E>):
 *      Inside the LinkedList class, there is an inner class Node<E>. Each Node represents an element in the linked list, containing data of type E and a reference to the next node.
 *
 *  3. Constructor (LinkedList()):
 *      The constructor initializes an empty linked list by setting the head reference to null and the size to 0.
 *
 *  4. getSize() Method:
 *      The getSize() method returns the current number of elements in the linked list.
 *
 *  5. insert(E data) Method:
 *      The insert method adds a new element with the given data to the end of the linked list. It traverses the list to find the last node and appends the new node to it.
 *
 *  6. getElement(E data) Method:
 *      The getElement method searches for an element with the specified data and returns it if found. If not found, it throws a NoSuchElementException.
 *
 *  7. getAt(int index) Method:
 *      The getAt method retrieves an element at a specified index in the linked list. It traverses the list to find the element at the given index and returns it. If the index is out of bounds, it throws a NoSuchElementException.
 *
 *  8. deleteAt(int index) Method:
 *      The deleteAt method removes an element at a specified index in the linked list. It updates the references of adjacent nodes to bypass the node at the specified index.
 *
 *  9. delete(E data) Method:
 *      The delete method removes an element with the specified data from the linked list. It checks if the element is at the head or elsewhere in the list and updates the references accordingly.
 *
 *  10. search(E data) Method:
 *      The search method searches for an element with the specified data and returns its index in the linked list. If the element is not found, it returns -1.
 *
 *  11. displayAll() Method:
 *      The displayAll method iterates through the linked list and prints all the elements.
 *
 *  12. join(LinkedList<E> listToJoin) Method:
 *      The join method joins the current linked list with another linked list. It appends the other list to the end of the current list, updates the size, and clears the other list.
 *
 *  13. update(E oldData, E newData) Method:
 *      The update method finds and updates an element with old data to new data in the linked list. If the element is found and updated, it returns true. Otherwise, it returns false.
 */

package GroupProject2;

import java.util.NoSuchElementException;

public class LinkedList<E> implements MyList<E> {
    private Node<E> head;
    private int size;

    // Inner class for the nodes of the linked list
    private static class Node<E> {
        E data; // data stored in the node
        Node<E> next; // Reference to the next node in the list

        // Constructor for creating a new node with data
        Node(E data) {
            this.data = data; // Initialize data
            this.next = null; // Initialize next reference to null
        }
        public void setNext(Node<E> next) {
            this.next = next;
        }
        public void setData(E data) {
            this.data = data;
        }

        public Node<E> getNext() {
            return next;
        }
        public E getData() {
            return data;
        }
    }
    // Constructor for initializing an empty linked list
    public LinkedList() {
        head = null; // Initialize the head to null
        size = 0; // Initialize the size to 0
    }
    // Get the current number of elements
    @Override
    public int getSize() {
        return size;
    }

    // Insertion a new element with the given data
    @Override
    public void insert(E data) throws ListOverflowException {
        // Create a new node with the given data
        Node<E> newNode = new Node<>(data);

        // If the list is empty, make the new node the head
        if (head == null) {
            head = newNode;
        } else {
            // Otherwise, traverse to the end of the list and add the new node there
            Node<E> current = head;
            while (current.next != null) {
                current = current.next;
            }
            current.next = newNode;
        }

        size++;
    }

    @Override
    public E getElement(E data) throws NoSuchElementException {
        Node<E> current = head;
        while (current != null) {
            // Check if the data in the current node matches the data
            if (current.data.equals(data)) {
                return current.data; // Return the found element
            }
            current = current.next; // Move to the next node
        }
        // Throw a NoSuchElementException if the element is not found
        throw new NoSuchElementException("Element not found: " + data);
    }
    public E getAt (int index)throws NoSuchElementException{
        if(index<0)
            throw new NoSuchElementException("No such element");
        Node n =head;
        for(int i=0;i<index;i++){
            n=n.getNext();
        }
        if(n==null)
            throw new NoSuchElementException("No such element");
        return (E) n.getData();
    }
    public void deleteAt(int index){
        size--;
        Node n =head;
        Node delete =null;
        if(index==0){
            head=head.getNext();
        }else{
            for(int i=0;i<index-1;i++){
                n=n.getNext();
            }
            delete = n.getNext();
            n.setNext(delete.getNext());
        }
    }

    @Override
    public boolean delete(E data) {
        // If list is empty, nothing to delete
        if (head == null) {
            return false;
        }
        // Check if the element to delete is at the head
        if (head.data.equals(data)) {
            head = head.next;
            size--;
            return true;
        }
        // Traverse to find and delete the element
        Node<E> current = head;
        while (current.next != null) {
            if (current.next.data.equals(data)) {
                current.next = current.next.next;
                size--;
                return true;
            }
            current = current.next;
        }
        // Element not found, return false
        return false;
    }

    @Override
    public int search(E data) {
        // Search for the given data in the linked list
        Node<E> current = head;
        int index = 0;
        while (current != null) {
            if (current.data.equals(data)) {
                return index; // Return the index of the found element
            }
            current = current.next;
            index++;
        }
        return -1; // Return -1 if the element is not found
    }
    // Display elements in the linked list
    public void displayAll() {
        Node<E> current = head;
        System.out.println("Elements in the linked list:");
        while (current != null) {
            System.out.println(current.data); // Print current element
            current = current.next; // Move to the next element
        }
    }
    public LinkedList<E> join(LinkedList<E> listToJoin) {
        if (listToJoin == null || listToJoin.head == null) {
            return this; // If the list to join is empty, no changes are needed
        }

        if (this.head == null) {
            this.head = listToJoin.head; // If this list is empty, set its head to the head of the list to join
        } else {
            Node<E> current = this.head;
            while (current.next != null) {
                current = current.next;
            }
            current.next = listToJoin.head; // Traverse to the end of this list and append the other list
        }

        this.size += listToJoin.size; // Update the size of this list

        listToJoin.head = null; // Clear the head of the list to join
        listToJoin.size = 0; // Reset the size of the list to join

        return this; // Return the modified list
    }

    // Update an element in the linked list with new data
    public boolean update(E oldData, E newData) {
        Node<E> current = head;

        while (current != null) {
            if (current.data.equals(oldData)) {
                current.data = newData; // Update the data of the current node
                return true;
            }
            current = current.next;
        }
        return false; // Element not found in the list
    }
}
