/**
 * Author : GroupFrostBourne
 * Title : PrelimGroupProject2
 *
 * Process :
 *  1. Interface Definition (MyList<E>):
 *      The code defines a generic interface named MyList with a type parameter E. This interface specifies a set of methods and a custom exception.
 *
 *  2. getSize() Method:
 *      The getSize method is declared in the interface. It represents a method to get the size (number of elements) of the list. Classes implementing this interface will provide their own implementation.
 *
 *  3. insert(E data) Method:
 *      The insert method is declared in the interface. It represents a method to insert an element of type E into the list. It may throw a custom ListOverflowException if the list is full. Classes implementing this interface will provide their own implementation of this method.
 *
 *  4. getElement(E data) Method:
 *      The getElement method is declared in the interface. It represents a method to retrieve an element of type E from the list based on the provided data. It may throw a NoSuchElementException if the element is not found. Classes implementing this interface will provide their own implementation.
 *
 *  5. delete(E data) Method:
 *      The delete method is declared in the interface. It represents a method to delete an element of type E from the list based on the provided data. It returns true if the element was successfully deleted, and false otherwise. Classes implementing this interface will provide their own implementation.
 *
 *  6. search(E data) Method:
 *      The search method is declared in the interface. It represents a method to search for an element of type E in the list based on the provided data. It returns the index of the first occurrence of the element, or -1 if the element is not found. Classes implementing this interface will provide their own implementation.
 *
 *  7. Nested Exception Class (ListOverflowException):
 *      The code defines a nested class within the interface named ListOverflowException. This is a custom exception class that extends RuntimeException. It is used to handle exceptions related to list overflow (e.g., when attempting to insert into a full list).
 *
 *  8. ListOverflowException Constructor:
 *      The ListOverflowException class has a constructor that accepts an error message as a parameter and passes it to the superclass (RuntimeException) constructor.
 *
 */

package GroupProject2;

import java.util.NoSuchElementException;

// Define a generic interface MyList with type parameter E
public interface MyList<E> {

    // Method to get the size of the list
    public int getSize();

    // Method to insert an element of type E into the list
    // Throws ListOverflowException if the list is full
    public void insert(E data) throws ListOverflowException;

    // Method to get an element of type E from the list
    // Throws NoSuchElementException if the element is not found
    public E getElement(E data) throws NoSuchElementException;

    // Method to delete an element of type E from the list
    // Returns true if the element was successfully deleted, false otherwise
    public boolean delete(E data);

    // Method to search for an element of type E in the list
    // Returns the index of the first occurrence of the element, or -1 if not found
    public int search(E data);

    // Nested class ListOverflowException, which is a custom exception
    class ListOverflowException extends RuntimeException {

        // Constructor for ListOverflowException
        public ListOverflowException(String errorMessage) {
            super(errorMessage);
        }
    }
}
