/**
 * Author : GroupFrostBourne
 * Title : PrelimGroupProject2
 *
 * Process :
 *  1. Class Definition:
 *      The code defines a class named GClassMenu within the GroupProject2 package. This class serves as the main class for managing classrooms and their operations.
 *
 *  2. Static Variables:
 *      The class defines several static variables, including lists for storing classrooms, an instance of the Classroom class, panels for displaying information, and counters to track the number of classrooms and search operations.
 *
 *  3. Main Method:
 *      The main method is the entry point of the program.
 *      It initializes various components of the user interface, including creating a JFrame window, setting its properties, creating panels for displaying classrooms and search functionality, and setting up menu options.
 *
 *  4. Create Classroom:
 *      The program allows users to create classrooms by selecting "Create Class" from the menu.
 *      It creates a panel (createPanel) for entering classroom information such as name, description, and code.
 *      When the user clicks the "Submit" button, the program validates the input and creates a Classroom object with the provided information. It then adds the classroom to the list, updates the display, and shows a success message.
 *
 *  5. View Classroom:
 *      The program allows users to view the list of classrooms by selecting "View Class" from the menu.
 *      It checks if the classroom list is empty and displays an error message if it is not.
 *
 *  6. Delete Classroom:
 *      Users can delete a classroom by selecting "Delete Class" from the menu.
 *      The program prompts the user to select a classroom from a list of available classrooms and deletes the selected classroom.
 *
 *  7. Update Classroom:
 *      Users can update a classroom by selecting "Update Class" from the menu.
 *      The program prompts the user to select a classroom from a list, enter new information for the classroom, and updates the classroom with the new information.
 *
 *  8. Search Classroom:
 *      Users can search for a classroom by selecting "Search class" from the menu.
 *      The program prompts the user to enter a classroom name to search for. It then searches for a matching classroom in the list and displays it in a panel.
 *
 *  9. UI Components:
 *      The code sets up various UI components, including labels, text fields, buttons, separators, and menus, to create the user interface for classroom management.
 *
 *  10. Image Resizing:
 *      There is a method resizeIcon that resizes an image icon to the specified width and height.
 *
 *  11. Create Classroom Panel:
 *      The createClassroom method creates a JPanel to represent a classroom with labels for name, description, and code. It sets fonts and preferred dimensions for the labels.
 *
 *  12. User Interaction:
 *      The program uses ActionListeners to respond to user actions such as clicking menu items and buttons.
 *
 *  13. Display Management:
 *      The program hides and shows panels as needed to update the interface when creating, viewing, updating, or searching for classrooms.
 */
package GroupProject2;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

// This is the main class for managing classrooms and their operations.
public class GClassMenu {

    // A list to store a list of classrooms. Each element of this list is a list of classrooms.
    static LinkedList<LinkedList<Classroom>> mainList = new LinkedList<>();
    // A list to store individual classrooms.
    static LinkedList<Classroom> clasroomList = new LinkedList<>();
    // An instance of the Classroom class to work with individual classrooms.
    Classroom classroom = new Classroom();
    // A list to store JPanel elements.
    static LinkedList<JPanel> panels = new LinkedList<>();
    // Counts the total number of classrooms.
    static int classroomCount =0;
    // Counts the number of search operations performed.
    static int searchCount=0;
    //Menu items for various operations.
    static JMenu Create, Read, Update, Delete, Search;

    static JMenuItem Createclass, Readclass, DeleteClass, Updateclass, searchClass;
    // Temporary variable to work with classrooms.
    static Classroom temp;

    public static void main(String[] args) {
        // Insert the classroom list into the main list.
        mainList.insert(clasroomList);

        // Create a JFrame to serve as the main application window.
        JFrame frame = new JFrame("Google Classroom");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        frame.setSize(800, 600);
        frame.setResizable(false);

        // Create a JPanel to display the list of classrooms.
        JPanel classroomListPanel = new JPanel();
        classroomListPanel.setLayout(new FlowLayout());
        classroomListPanel.setBounds(0,70,800,600);

        // Create a JPanel for search functionality.
        JPanel searchPanel = new JPanel();
        searchPanel.setLayout(null);

        // Create a JLabel for the application title.
        JLabel title = new JLabel("<html><span style='color: gray; '>Classroom</span></html>"); // Set the color to gray
        title.setBounds(50, 20, 400, 30); // Adjusted the x-coordinate from 150 to 50
        title.setFont(title.getFont().deriveFont(20.0f));

        // Create a JLabel for the Google Classroom logo.
        JLabel imageLabel = new JLabel();
        ImageIcon icon = new ImageIcon("google-classroom-logo-2.png");
        ImageIcon resizedImage = resizeIcon(icon, 35, 30);
        imageLabel.setBounds(10, 20, resizedImage.getIconWidth(), resizedImage.getIconHeight()); // Adjusted the x-coordinate from 100 to 10
        imageLabel.setIcon(resizedImage);

        // Add a separator line
        JSeparator separator = new JSeparator();
        separator.setBounds(0, 71, 800, 1); // Adjusted the y-coordinate to lower the line
        separator.setForeground(new Color(200, 200, 200));

        // Add the separator to the frame
        frame.add(separator);

        // Create a JMenuBar for menu options.
        JMenuBar mb = new JMenuBar();

        // Create individual menus for different operations.
        Create = new JMenu("Create");
        Read = new JMenu("Read");
        Update = new JMenu("Update");
        Delete = new JMenu("Delete");
        Search = new JMenu("Search");

        // Create a menu item for creating a new class.
        Createclass = new JMenuItem("Create Class");

        // Create a panel for the "Create" functionality.
        JPanel createPanel = new JPanel();
        createPanel.setLayout(null);
        createPanel.setBounds(2,70,780,460);
        createPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        // Add an ActionListener to the "Createclass" JMenuItem.
        Createclass.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // When the "Create Class" option is selected, make the "createPanel" visible.
                createPanel.setVisible(true);
                // Create a JLabel for the title of the create class section.
                JLabel titleC = new JLabel("CREATE CLASS");
                titleC.setBounds(345,60,300,30);
                titleC.setFont(titleC.getFont().deriveFont(15.0f));
                createPanel.add(titleC);
                titleC.revalidate();
                titleC.repaint();

                // Create a JLabel for entering the class name.
                JLabel classname = new JLabel("Enter Classname: ");
                classname.setBounds(220,95,300,30);
                createPanel.add(classname);
                classname.revalidate();
                classname.repaint();

                // Create a JTextField for entering the class name.
                JTextField Ntextf = new JTextField();
                Ntextf.setBounds(250,120,300,30);
                createPanel.add(Ntextf);
                Ntextf.revalidate();
                Ntextf.repaint();

                // Create a JLabel for entering the class description.
                JLabel classdesc = new JLabel("Enter Class Description: ");
                classdesc.setBounds(220,170,300,30);
                createPanel.add(classdesc);
                classdesc.revalidate();
                classdesc.repaint();

                // Create a JTextField for entering the class description.
                JTextField dtextf = new JTextField();
                dtextf.setBounds(250,195,300,30);
                createPanel.add(dtextf);
                dtextf.revalidate();
                dtextf.repaint();

                // Create a JLabel for entering the class code.
                JLabel classcode = new JLabel("Enter Classcode: ");
                classcode.setBounds(220,245,300,30);
                createPanel.add(classcode);
                classcode.revalidate();
                classcode.repaint();

                // Create a JTextField for entering the class code.
                JTextField ctextf = new JTextField();
                ctextf.setBounds(250,270,300,30);
                createPanel.add(ctextf);
                ctextf.revalidate();
                ctextf.repaint();

                // Create a JButton for submitting the class creation.
                JButton submit = new JButton("Submit");
                submit.setBounds(350,320,100,40);
                createPanel.add(submit);
                submit.repaint();
                submit.revalidate();

                // Add an ActionListener to the "Submit" button.
                submit.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        // Retrieve the input values entered by the user.
                        String getclassname = Ntextf.getText();
                        String getclassdescription = dtextf.getText();
                        String getClasscode = ctextf.getText();

                        // Check if any of the input fields is empty.
                        if (getclassname.isEmpty()) {
                            // Display a message if the class name input is empty.
                            JOptionPane.showMessageDialog(null,"Input is empty");
                        } else if (getclassdescription.isEmpty()) {
                            // Display a message if the class description input is empty.
                            JOptionPane.showMessageDialog(null,"Input is empty");
                        } else if (getClasscode.isEmpty()) {
                            // Display a message if the class code input is empty.
                            JOptionPane.showMessageDialog(null,"Input is empty");
                        } else {
                            // If all inputs are valid, display a success message.
                            JOptionPane.showMessageDialog(null,"Created Successfully!");

                            // Hide the create panel.
                            createPanel.setVisible(false);
                            // Clear the input fields.
                            Ntextf.setText("");
                            dtextf.setText("");
                            ctextf.setText("");
                            // Create a new Classroom object with the provided inputs.
                            temp = new Classroom(getclassname,getclassdescription,getClasscode);
                            // Insert the new Classroom object into the classroom list.
                            clasroomList.insert(temp);
                            // Create a JPanel to display the newly created classroom and add it to the classroom list panel.
                            classroomListPanel.add(createClassroom(temp));
                            // Add the JPanel representing the new classroom to the panels list.
                            panels.insert(createClassroom(temp));
                            // Hide the classroom list panel and the search panel (if they were visible).
                            classroomListPanel.setVisible(false);
                            searchPanel.setVisible(false);
                        }
                    }
                });
            }
        });

        // Create a JMenuItem for viewing a class.
        Readclass = new JMenuItem("View Class");
        // Add an ActionListener to the "View Class" JMenuItem.
        Readclass.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Check if the classroom list is empty.
                if(clasroomList.getSize()==0){
                    // Display an error message if the list is empty.
                    JOptionPane.showMessageDialog(null,"List is empty","Empty",JOptionPane.ERROR_MESSAGE);
                }
                // Hide and then show the classroom list panel to refresh its contents.
                classroomListPanel.setVisible(false);
                classroomListPanel.setVisible(true);
            }
        });

        // Create a JMenuItem for deleting a class.
        DeleteClass = new JMenuItem("Delete Class");
        // Add an ActionListener to the "Delete Class" JMenuItem.
        DeleteClass.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String option= "";
                LinkedList<Classroom> gclass = null;
                // Iterate through the mainList to retrieve the list of classrooms
                for(int i=0;i<mainList.getSize();i++){
                    gclass = mainList.getAt(i);
                }
                // Check if the list of classrooms is empty.
                if(gclass.getSize()==0){
                    // Display an error message if the list is empty.
                    JOptionPane.showMessageDialog(null,"List is empty","Empty",JOptionPane.ERROR_MESSAGE);
                    return;
                }
                // Create a string representation of the available classrooms for user selection.
                for(int i=0;i<gclass.getSize();i++){
                    option+= (i+1)+". "+gclass.getAt(i)+"\n";
                }
                boolean stop = false;
                int numChoice=0;
                //loop when input string or invalid number
                while(!stop) {
                    try {
                        // Prompt the user to enter a number to delete a classroom.
                        String choice = JOptionPane.showInputDialog("Enter a number to delete classroom\n\n" + option);
                        // Check if the user cancels the operation.
                        if(choice==null){
                            return;
                        }
                        numChoice= Integer.parseInt(choice);
                        stop=true;
                        // Check if the user entered a valid number within the range.
                        if(numChoice<1 || numChoice > gclass.getSize()){
                            JOptionPane.showMessageDialog(null,"Please input a number from 1 to "+gclass.getSize()+"!","Error",JOptionPane.ERROR_MESSAGE);
                            stop=false;
                        }else{
                            stop=true;
                        }
                    }catch (Exception ex){
                        System.out.println(ex);
                        JOptionPane.showMessageDialog(null,"Please input numbers only!","Error",JOptionPane.ERROR_MESSAGE);
                    }
                }
                // Remove the selected classroom from the classroomListPanel based on user input
                classroomListPanel.remove((numChoice-1));
                // Delete the JPanel representing the classroom from the panels list.
                panels.deleteAt((numChoice-1));
                // Delete the selected classroom from the gclass (mainList) based on user input.
                gclass.deleteAt((numChoice-1));
                // Decrement the classroomCount to reflect the updated number of classrooms.
                classroomCount--;
                // Display a success message to inform the user that the deletion was successful.
                JOptionPane.showMessageDialog(null,"Delete successful","Delete",JOptionPane.INFORMATION_MESSAGE);
                //Panel set visible false, and true to update the interface when deleting a classroom
                classroomListPanel.setVisible(false);
                searchPanel.setVisible(false);
            }
        });

        // Create a JMenuItem for updating a class.
        Updateclass = new JMenuItem("Update Class");
        // Add an ActionListener to the "Update Class" JMenuItem.
        Updateclass.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String option= "";
                LinkedList<Classroom> gclass = null;
                // Iterate through the mainList to retrieve the list of classrooms.
                for(int i=0;i<mainList.getSize();i++){
                    gclass = mainList.getAt(i);
                }
                // Check if the list of classrooms is empty.
                if(gclass.getSize()==0){
                    // Display an error message if the list is empty.
                    JOptionPane.showMessageDialog(null,"List is empty","Empty",JOptionPane.ERROR_MESSAGE);
                    return;
                }
                // Create a string representation of the available classrooms for user selection.
                for(int i=0;i<gclass.getSize();i++){
                    option+= (i+1)+". "+gclass.getAt(i)+"\n";
                }
                boolean stop = false;
                int numChoice=0;
                //loop when input string, or invalid number
                while(!stop) {
                    try {
                        // Prompt the user to enter a number to update a classroom.
                        String choice = JOptionPane.showInputDialog("Enter a number to update classroom\n\n" + option);
                        // Check if the user cancels the operation.
                        if(choice==null){
                            return;
                        }
                        numChoice= Integer.parseInt(choice);
                        stop=true;
                        // Check if the user entered a valid number within the range.
                        if(numChoice<1 || numChoice > gclass.getSize()){
                            JOptionPane.showMessageDialog(null,"Please input a number from 1 to "+gclass.getSize()+"!","Error",JOptionPane.ERROR_MESSAGE);
                            stop=false;
                        }else{
                            stop=true;
                        }
                    }catch (Exception ex){
                        System.out.println(ex);
                        JOptionPane.showMessageDialog(null,"Please input numbers only!","Error",JOptionPane.ERROR_MESSAGE);
                    }
                }
                // Retrieve the classroom to be updated based on user input.
                Classroom temp = gclass.getAt((numChoice-1));
                // Prompt the user to enter new values for the classroom.
                String newName = JOptionPane.showInputDialog("Enter new name for the classroom");
                String newDesc = JOptionPane.showInputDialog("Enter new description for the classroom");
                String newCode = JOptionPane.showInputDialog("Enter new class code: ");
                // Update the classroom with the new values.
                temp.setCname(newName);
                temp.setCdescription(newDesc);
                temp.setCcode(newCode);
                // Retrieve the corresponding JPanel for the classroom.
                JPanel panelTemp =  panels.getAt((numChoice-1));
                // Create a new JPanel to represent the updated classroom.
                panelTemp = createClassroom(temp);
                // Remove the old classroom JPanel and add the updated one at the same position.
                classroomListPanel.remove((numChoice-1));
                classroomListPanel.add(panelTemp,((numChoice-1)));
                // Display a success message to inform the user that the update was successful.
                JOptionPane.showMessageDialog(null,"Update successful","Update",JOptionPane.INFORMATION_MESSAGE);
                //Panel set visible false, and true to update the interface when deleting a classroom
                classroomListPanel.setVisible(false);
                searchPanel.setVisible(false);
            }
        });

        // Create a JMenuItem for searching for a class.
        searchClass = new JMenuItem("Search class");
        // Set the initial bounds for the searchPanel.
        searchPanel.setBounds(0,70,800,600);

        // Add an ActionListener to the "Search class" JMenuItem.
        searchClass.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LinkedList<Classroom> gclass = null;
                // Iterate through the mainList to retrieve the list of classrooms.
                for(int i=0;i<mainList.getSize();i++){
                    gclass = mainList.getAt(i);
                }
                //Output empty if the List is empty
                if(gclass.getSize()==0){
                    JOptionPane.showMessageDialog(null,"List is empty","Empty",JOptionPane.ERROR_MESSAGE);
                    return;
                }
                // Prompt the user to enter a classroom name to search for.
                String search = JOptionPane.showInputDialog("Enter classroom name to search");
                //Cancels when clicked cancel or close window
                if(search==null){
                    return;
                }
                boolean success= false;
                Classroom searchClassroom = new Classroom(search);
                Classroom tempClassroom = null;

                // Iterate through the list of classrooms to find a match for the searched classroom name.
                for(int i=0;i<gclass.getSize();i++){
                    tempClassroom = gclass.getAt(i);
                    // Check if the current classroom matches the searched classroom name.
                    if(tempClassroom.equals(searchClassroom)){
                        success =true;
                        break;
                    }
                }
                // If a matching classroom is found, display it.
                if(success){
                    // Remove any previous search results if they exist.
                    if(searchCount==1){
                        searchPanel.remove(0);
                        searchCount--;
                    }
                    // Create a JPanel to represent the found classroom.
                    JPanel panelTemp = createClassroom(tempClassroom);
                    //Searched classroom placement
                    panelTemp.setBounds(145,125,500,100);
                    // Add the searched classroom JPanel to the searchPanel.
                    searchPanel.add(panelTemp);
                    // Increment the searchCount to indicate that a search result is displayed.
                    searchCount++;
                    // Hide the classroomListPanel and show the searchPanel to display the search result.
                    classroomListPanel.setVisible(false);
                    searchPanel.setVisible(true);
                }else{
                    // Display an error message if the classroom is not found.
                    JOptionPane.showMessageDialog(null,"Classroom not found!","Search",JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        // Add the "Createclass" JMenuItem to the "Create" menu.
        Create.add(Createclass);
        // Add the "DeleteClass" JMenuItem to the "Delete" menu.
        Delete.add(DeleteClass);
// Add the "Updateclass" JMenuItem to the "Update" menu.
        Update.add(Updateclass);
// Add the "Readclass" JMenuItem to the "Read" menu.
        Read.add(Readclass);
// Add the "searchClass" JMenuItem to the "Search" menu.
        Search.add(searchClass);

        // Add menus
        mb.add(Create);
        mb.add(Read);
        mb.add(Update);
        mb.add(Delete);
        mb.add(Search);

        // Add the createPanel and set it initially invisible.
        frame.add(createPanel);
        createPanel.setVisible(false);
        // Add the classroomListPanel.
        frame.add(classroomListPanel);
        // Add the searchPanel and set it initially invisible.
        frame.add(searchPanel);
        searchPanel.setVisible(false);
        // Add the title label and Google Classroom logo label.
        frame.add(title);
        frame.add(imageLabel);
        // Set the JFrame's location relative to the center of the screen.
        frame.setLocationRelativeTo(null);
        // Set the JMenuBar for the JFrame.
        frame.setJMenuBar(mb);
        // Make the JFrame visible.
        frame.setVisible(true);

    }

    // This method resizes an ImageIcon to the specified width and height.
    public static ImageIcon resizeIcon(ImageIcon icon, int width, int height) {
        Image image = icon.getImage();
        // Scale the image to the desired width and height with smooth scaling.
        Image resizedImage = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        // Create and return a new ImageIcon with the resized image.
        return new ImageIcon(resizedImage);
    }

    // This method creates a JPanel to represent a classroom.
    public static JPanel createClassroom(Classroom room){
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        // Create borders for the panel.
        Border margin = BorderFactory.createEmptyBorder(5,5,5,5);
        Border colorBorder = BorderFactory.createLineBorder(Color.BLACK);
        panel.setBorder(BorderFactory.createCompoundBorder(colorBorder,margin));
        // Create JLabels to display classroom information.
        JLabel name = new JLabel(room.getCname());
        JLabel desc = new JLabel(room.getCdescription());
        JLabel code = new JLabel(room.getCcode());
        code.setHorizontalAlignment(JLabel.CENTER);
        // Set font styles for the labels.
        name.setFont(new Font("Monospaced",Font.BOLD,20));
        desc.setFont(new Font("Monospaced",Font.PLAIN,15));
        code.setFont(new Font("Monospaced",Font.PLAIN,15));
        // Add labels to the panel in BorderLayout regions.
        panel.add(name,BorderLayout.NORTH);
        panel.add(desc,BorderLayout.CENTER);
        panel.add(code,BorderLayout.SOUTH);
        // Set the preferred size of the panel.
        panel.setPreferredSize(new Dimension(500,100));
        // Return the JPanel representing the classroom.
        return panel;
    }

}