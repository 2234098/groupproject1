package GroupProject2;


/**
 * Author : GroupFrostBourne
 * Title : PrelimGroupProject2
 *
 * Process :
 *
 *  1. Class Definition:
 *      The code defines a class named Classroom within the GroupProject2 package.
 *
 *  2. Private Instance Variables:
 *      The class has three private instance variables:
 *      Cname (class name)
 *      Cdescription (class description)
 *      Ccode (class code)
 *      These variables are declared as private, so they can only be accessed within the class.
 *
 *  3. Constructors:
 *      The class defines three constructors:
 *      The first constructor public Classroom(String Cname, String Cdescription, String Ccode) allows you to initialize the class information (name, description, and code) when an object of the Classroom class is created.
 *      The second constructor public Classroom() is a default constructor that sets all class information to null.
 *      The third constructor public Classroom(String Cname) allows you to initialize only the Cname field while setting Cdescription and Ccode to null.
 *
 *  4.toString Methods:
 *      The class defines two toString methods:
 *      The first static method public static String toString(String getclassname, String getclassdescription, String getClasscode) is a static method that takes three strings as parameters and returns a formatted string representation of class information.
 *      The second toString method @Override public String toString() overrides the toString method inherited from the Object class. It returns a string representation of the class by calling getCname() and getCcode() methods.
 *
 *  5.Setter and Getter Methods:
 *      The class defines setter methods (setCname, setCdescription, and setCcode) to set the values of the private instance variables.
 *      It also defines getter methods (getCname, getCdescription, and getCcode) to retrieve the values of these variables. These getter methods return formatted strings containing the corresponding class information.
 *
 *  6. equals Method:
 *      The class defines an equals method, which is used to compare two Classroom objects for equality.
 *      It checks if the Cname of the current object is equal to the Cname of the object passed as a parameter.
 *
 */
public class Classroom {
    //Private static variables to store class information
    private String Cname;
    private String Cdescription;
    private String Ccode;

    // Constructor to initialize class information
    public Classroom(String Cname, String Cdescription, String Ccode) {
        this.Cname = Cname;
        this.Cdescription = Cdescription;
        this.Ccode = Ccode;
    }
    // Default constructor to set class information to null
    public Classroom() {
        Cname = null;
        Cdescription=null;
        Ccode=null;
    }
    public Classroom(String Cname){
        this.Cname = Cname;
        Cdescription=null;
        Ccode=null;
    }
    // Static method to generate a string representation of class information
    public static String toString(String getclassname, String getclassdescription, String getClasscode) {
        return "Classname: " + getclassname + "\nClass Description: " + getclassdescription + "\nClass Code: " + getClasscode;
    }
    @Override
    public String toString() {
        return getCname()+" ; "+getCcode();
    }
    public void setCname(String cname) {
        Cname = cname;
    }
    public void setCdescription(String cdescription) {
        Cdescription = cdescription;
    }
    public void setCcode(String ccode) {
        Ccode = ccode;
    }
    // Getter method for class name
    public  String getCname() {
        return "Classname: " + Cname;
    }
    // Getter method for class description
    public  String getCdescription() {
        return "Class Description: " + Cdescription;
    }
    // Static getter method for class code
    public  String getCcode() {
        return "Class Code: " + Ccode;
    }

    public boolean equals(Object obj) {
        Classroom classroom = (Classroom) obj;
        if(this.Cname.equals(classroom.Cname)){
            return true;
        }else{
            return false;
        }
    }
}