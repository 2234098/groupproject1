package DataSet50k;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Output50k {
    public static void main(String[] args) {
        String fileBest="src/DataSet50k/Bestcase50k.txt";
        String fileWorst="src/DataSet50k/Worstcase50k.txt";
        String fileAverage="src/DataSet50k/Averagecase50k.txt";
        long[] numbersBestBubble = new long[countLinesInFile(fileBest)];
        long[] numbersWorstBubble = new long[countLinesInFile(fileWorst)];
        long[] numbersAverageBubble = new long[countLinesInFile(fileWorst)];
        populateArray(numbersBestBubble,fileBest);
        populateArray(numbersWorstBubble,fileWorst);
        populateArray(numbersAverageBubble,fileAverage);

        long[] numbersBestSelection = new long[countLinesInFile(fileBest)];
        long[] numbersWorstSelection = new long[countLinesInFile(fileWorst)];
        long[] numbersAverageSelection  = new long[countLinesInFile(fileAverage)];
        populateArray(numbersBestSelection ,fileBest);
        populateArray(numbersWorstSelection ,fileWorst);
        populateArray(numbersAverageSelection ,fileAverage);

        long[] numbersBestInsertion = new long[countLinesInFile(fileBest)];
        long[] numbersWorstInsertion = new long[countLinesInFile(fileWorst)];
        long[] numbersAverageInsertion = new long[countLinesInFile(fileAverage)];
        populateArray(numbersBestInsertion,fileBest);
        populateArray(numbersWorstInsertion,fileWorst);
        populateArray(numbersAverageInsertion,fileAverage);

        System.out.println("Table 2. Number of statements executed for each algorithm with 50,000 data");
        System.out.println();
        System.out.println("Algorithm\t\t\tBest Case\tWorst Case\t\tAverage Case");
        System.out.println("Bubble Sort:\t\t"+bubbleSort(numbersBestBubble)+"\t\t"+bubbleSort(numbersWorstBubble)+"\t\t"+bubbleSort(numbersAverageBubble));
        System.out.println("Selection Sort:\t\t"+selectionSort(numbersBestSelection)+"\t\t\t"+selectionSort(numbersWorstSelection)+"\t\t"+selectionSort(numbersAverageSelection));
        System.out.println("Insertion Sort:\t\t"+insertionSort(numbersBestInsertion)+"\t\t\t"+insertionSort(numbersWorstInsertion)+"\t\t"+insertionSort(numbersAverageInsertion));
    }
    public static int countLinesInFile(String fileName){
        int linesCount=0;
        try {
            Scanner scanner = new Scanner(new File(fileName));
            while(scanner.hasNextLine()){
                scanner.nextLine();
                linesCount++;
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        return linesCount;
    }
    public static void populateArray(long[] array, String fileName){
        try {
            Scanner scanner = new Scanner(new File(fileName));
            while(scanner.hasNextLine()){
                for(int i=0;i<array.length;i++){
                    array[i] = Integer.parseInt(scanner.nextLine());
                }
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
    public static long bubbleSort(long[] array) {
        long count =0;
        for(int i=0;i<array.length-1;i++){
            boolean swap = false;
            for(int y=0; y<array.length-1-i;y++){
                if(array[y]>array[y+1]){
                    long temp = array[y];
                    array[y] = array[y+1];
                    array[y+1] = temp;
                    swap=true;
                }
                count++;
            }
            if(!swap){
                break;
            }
        }
        return count;
    }
    public static long insertionSort(long[] array){
        long count =0;
        for(int i=1;i<array.length;i++){
            long key = array[i];
            int y= i-1;
            while(y>=0 && key<array[y]){
                long temp = array[y];
                array[y] = array[y+1];
                array[y+1] = temp;
                y--;
                count++;
            }
        }
        return count;
    }
    public static long selectionSort(long[] array){
        long count=0;
        for(int i=0;i<array.length-1;i++){
            int minIndex = i;
            for(int y =i+1;y<array.length;y++){
                if(array[y]<array[minIndex]){
                    minIndex = y;
                    count++;
                }
            }
            long temp = array[i];
            array[i] = array[minIndex];
            array[minIndex] = temp;
        }
        return count;
    }
}
