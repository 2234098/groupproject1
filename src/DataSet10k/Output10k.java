package DataSet10k;
import java.io.*;
import java.util.*;
import java.text.SimpleDateFormat;
public class Output10k {
    public static void main(String[] args) {

            // Define the file paths for the input text files
            String ascendingFilePath = "src/DataSet10k/Bestcase10k.txt";
            String descendingFilePath = "src/DataSet10k/Worstcase10k.txt";
            String randomFilePath = "src/DataSet10k/Averagecase10k.txt";

            // Read data from the text files
            int[] ascendingArray = readFromFile(ascendingFilePath);
            int[] descendingArray = readFromFile(descendingFilePath);
            int[] randomArray = readFromFile(randomFilePath);

            // Perform sorting and count executions
            int bubbleSortAscExecutions = bubbleSort(Arrays.copyOf(ascendingArray, ascendingArray.length));
            int bubbleSortDescExecutions = bubbleSort(Arrays.copyOf(descendingArray, descendingArray.length));
            int bubbleSortRandomExecutions = bubbleSort(Arrays.copyOf(randomArray, randomArray.length));

            int selectionSortAscExecutions = selectionSort(Arrays.copyOf(ascendingArray, ascendingArray.length));
            int selectionSortDescExecutions = selectionSort(Arrays.copyOf(descendingArray, descendingArray.length));
            int selectionSortRandomExecutions = selectionSort(Arrays.copyOf(randomArray, randomArray.length));

            int insertionSortAscExecutions = insertionSort(Arrays.copyOf(ascendingArray, ascendingArray.length));
            int insertionSortDescExecutions = insertionSort(Arrays.copyOf(descendingArray, descendingArray.length));
            int insertionSortRandomExecutions = insertionSort(Arrays.copyOf(randomArray, randomArray.length));

            // Display results in a table format and results
            System.out.println("Table 1. Number of statements executed for each algorithm with 10,000 data");
            System.out.println();
            System.out.println("Algorithm\t\tBest Case\tWorst Case\tAverage Case");
            System.out.println("Bubble Sort\t\t" + bubbleSortAscExecutions + "\t\t" + bubbleSortDescExecutions + "\t\t" + bubbleSortRandomExecutions);
            System.out.println("Selection Sort\t" + selectionSortAscExecutions + "\t" + selectionSortDescExecutions + "\t\t" + selectionSortRandomExecutions);
            System.out.println("Insertion Sort\t\t" + insertionSortAscExecutions + "\t\t" + insertionSortDescExecutions + "\t\t" + insertionSortRandomExecutions);
        }

        // Read integers from a text file and return as an array
        private static int[] readFromFile(String filePath) {
            try {
                BufferedReader reader = new BufferedReader(new FileReader(filePath));
                String line;
                String[] numbers;
                StringBuilder content = new StringBuilder();

                while ((line = reader.readLine()) != null) {
                    content.append(line).append("\n");
                }
                reader.close();

                numbers = content.toString().trim().split("\n");
                int[] result = new int[numbers.length];
                for (int i = 0; i < numbers.length; i++) {
                    result[i] = Integer.parseInt(numbers[i]);
                }
                return result;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        // Implement bubble sort and count executions
        private static int bubbleSort(int[] arr) {
            int n = arr.length;
            int executions = 0;
            boolean swapped;
            do {
                swapped = false;
                for (int i = 1; i < n; i++) {
                    if (arr[i - 1] > arr[i]) {
                        int temp = arr[i - 1];
                        arr[i - 1] = arr[i];
                        arr[i] = temp;
                        swapped = true;
                    }
                    executions++;
                }
                n--;
            } while (swapped);
            return executions;
        }

        // Implement selection sort and count executions
        private static int selectionSort(int[] arr) {
            int n = arr.length;
            int executions = 0;
            for (int i = 0; i < n - 1; i++) {
                int minIndex = i;
                for (int j = i + 1; j < n; j++) {
                    if (arr[j] < arr[minIndex]) {
                        minIndex = j;
                    }
                    executions++;
                }
                int temp = arr[i];
                arr[i] = arr[minIndex];
                arr[minIndex] = temp;
            }
            return executions;
        }

        // Implement insertion sort and count executions
        private static int insertionSort(int[] arr) {
            int n = arr.length;
            int executions = 0;
            for (int i = 1; i < n; i++) {
                int key = arr[i];
                int j = i - 1;
                while (j >= 0 && arr[j] > key) {
                    arr[j + 1] = arr[j];
                    j--;
                    executions++;
                }
                arr[j + 1] = key;
            }
            return executions;
        }
}
