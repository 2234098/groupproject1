package IT212_9443_midGroupA.midlab1;

public class StackException extends RuntimeException{
    // Calls constructor of class
    /**
     * @param message The error message describing the exception.
     */
    public StackException(String message){
        super(message);
    }
} // end of class StackUnderflowException
