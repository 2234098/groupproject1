package IT212_9443_midGroupA.midlab1;

public class Node<T> {
    // Declare the variables
    private T datum;
    private Node<T> next;

    /**
     * Empty Constructor
     */
    public Node() {

    }
    /**
     * Constructor with data and reference
     * @param datum object of T
     * @param next reference link
     */
    public Node(T datum, Node<T> next) {
        this.datum = datum;
        this.next = next;
    }

    /**
     * @return data
     */
    public T getDatum() {
        return datum;
    }

    /**
     * sets info for the node
     * @param datum
     */
    public void setDatum(T datum) {
        this.datum = datum;
    }

    /**
     * returns link of the node
     * @return
     */
    public Node<T> getNext() {
        return next;
    }

    /**
     * sets link of the node
     * reference link
     * @param n
     */
    public void setNext(Node<T> n) {
        next = n;
    }
} // end of Node class
