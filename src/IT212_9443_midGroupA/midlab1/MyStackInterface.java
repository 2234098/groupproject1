package IT212_9443_midGroupA.midlab1;

public interface MyStackInterface<T> {

    /**
     * Pushes an item onto the stack.
     *
     * @param item The item to be pushed onto the stack.
     * @throws StackException Thrown if the stack is full (StackUnderflowException).
     */
    public void push(T item) throws StackException;
    // Adds item to the stack, throws StackUnderflowException if list is full

    /**
     * Removes and returns the top element from the stack.
     *
     * @return The top element of the stack.
     * @throws StackException Thrown if the stack is empty.
     */
    public T pop() throws StackException;
    // Removes the top element from the list

    /**
     * Returns the top element of the stack without removing it.
     *
     * @return The top element of the stack.
     * @throws StackException Thrown if the stack is empty.
     */
    public T peek() throws StackException;
    //returns the top from the stack

    /**
     * Returns the size of the stack.
     *
     * @return The size of the stack.
     */
    public int size();
    //Returns the size of the stack

    /**
     * Checks if the stack is empty.
     *
     * @return True if the stack is empty, false otherwise.
     */

    public boolean isEmpty();
    //Checks if stack is empty

    /**
     * Deletes an element from the stack (not specified in the method signature).
     */
    public void delete();
    //Deletes an element from the list

} //end of MyStackInterface
