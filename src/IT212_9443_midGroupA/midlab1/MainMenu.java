/**
 * Authors: Group A
 * Title: Group Programming Project 1 for Data Structures (Midterm)
 *
 * Algorithm:
 *
 * 1. Display a menu to the user with options to:
 *    a. Evaluate a postfix expression
 *    b. Convert an infix expression to postfix
 *    c. Exit the program
 *
 * 2. If the user closes the dialog, exit the program.
 *
 * 3. If the user selects option "1":
 *    a. Prompt the user to enter a postfix expression.
 *    b. Evaluate the postfix expression using a stack.
 *    c. Display the result in a table along with the evaluation steps.
 *    d. Handle exceptions if the expression is invalid or there is an error.
 *
 * 4. If the user selects option "2":
 *    a. Prompt the user to enter an infix expression.
 *    b. Check if the infix expression is valid in terms of brackets and characters.
 *    c. Convert the infix expression to postfix using a stack.
 *    d. Display the conversion steps in a table.
 *    e. Handle exceptions if the expression is invalid.
 *
 * 5. If the user selects option "3," exit the program.
 *
 * 6. Continue the loop until the user decides to exit.
 */
import IT212_9443_midGroupA.midlab1.MyStack;
import IT212_9443_midGroupA.midlab1.StackException;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
/**
 Project Title: Conversion of Infix to Postfix Expression and Evaluation of a Postfix Expression
 Group Name: A (Frostbourne)


 Algorithm:
 1. Start the main function.
 2. Set a boolean variable "running" to true.
 3. Enter a while loop with the condition "running" to create a menu-driven program.
 4. Display a menu to the user with options to:
 a. Evaluate a postfix expression
 b. Convert an infix expression to postfix
 c. Exit the program
 5. Read the user's choice.
 6. If the user closes the dialog, set "running" to false and exit the program.
 7. Use a switch statement to handle different choices:
 a. If the choice is "1":
 i. Prompt the user to enter a postfix expression.
 ii. Evaluate the postfix expression using a stack and store the result.
 iii. Create a table to display the postfix evaluation steps.
 iv. Show the table in a dialog box.
 v. Handle any exceptions that occur during evaluation and display an error message.
 b. If the choice is "2":
 i. Prompt the user to enter an infix expression.
 ii. Check if the infix expression is valid in terms of brackets and characters.
 iii. Convert the infix expression to postfix using a stack and store the result.
 iv. Create a table to display the conversion steps.
 v. Show the table in a dialog box.
 vi. Handle any exceptions that occur during conversion and display an error message.
 c. If the choice is "3", set "running" to false to exit the program.
 d. If the choice is invalid, display an error message.
 8. End the switch statement.
 9. End the while loop.
 10. End the main function.

 Additional Functions:
 1. Function evaluatePostfix(postfixExpression):
 a. Initialize a stack for operands.
 b. Iterate over each symbol in the postfix expression.
 c. If the symbol is an operand, push it to the stack.
 d. If the symbol is an operator, pop two operands from the stack, perform the operation, and push the result back to the stack.
 e. After iterating through all symbols, the final result will be at the top of the stack.
 f. Return the result.

 2. Function isOperator(symbol):
 a. Check if the symbol is one of the supported operators (+, -, *, /, ^, $).
 b. Return true if it is, false otherwise.

 3. Function precedence(operator1, operator2):
 a. Compare the precedence of two operators.
 b. Return true if operator1 has higher or equal precedence compared to operator2, false otherwise.

 4. Function convertToPostfixWithSteps(infixExpression):
 a. Initialize a stack for operators.
 b. Initialize an empty table model for conversion steps.
 c. Iterate over each symbol in the infix expression.
 d. If the symbol is an operand, append it to the postfix expression.
 e. If the symbol is an operator:
 i. Pop operators from the stack and append them to the postfix expression until an operator with lower precedence or an opening parenthesis is encountered.
 ii. Push the current operator to the stack.
 f. If the symbol is an opening parenthesis, push it to the stack.
 g. If the symbol is a closing parenthesis:
 i. Pop operators from the stack and append them to the postfix expression until an opening parenthesis is encountered.
 ii. Discard the opening parenthesis from the stack.
 h. Update the table model with the current symbol, postfix expression, and operator stack.
 i. After iterating through all symbols, pop any remaining operators from the stack and append them to the postfix expression.
 j. Return the table model.

 5. Function performOperation(operator, operand1, operand2):
 a. Perform the specified operation (+, -, *, /, ^) on the two operands.
 b. Return the result of the operation.

 6. Function validateBrackets(infixExpression):
 a. Initialize a stack for brackets.
 b. Iterate over each symbol in the infix expression.
 c. If the symbol is an opening bracket, push it to the stack.
 d. If the symbol is a closing bracket:
 i. If the stack is empty, return false (brackets are not balanced).
 ii. Pop the top bracket from the stack and compare it with the current closing bracket.
 iii. If they match, continue to the next symbol.
 iv. If they don't match, return false (brackets are not balanced).
 e. After iterating through all symbols, if the stack is empty, return true (brackets are balanced), otherwise return false.

 7. Function checkValid(infixExpression):
 a. Initialize a stack for tracking characters.
 b. Initialize a variable "valid" as false.
 c. Iterate over each character in the infix expression.
 d. If the character is a letteror digit, or an operand, continue to the next character.
 e. If the character is an operator, pop an element from the stack if it's not empty, otherwise return false.
 f. If the character is any other character (not a letter or digit, or an operand or operator), return false.
 g. After iterating through all characters, if the stack size is 1, set "valid" to false.
 h. Return the value of "valid".


 */
public class MainMenu {
    public static void main(String[] args) {
        boolean running = true;

        while (running) {
            String choice = JOptionPane.showInputDialog(null,
                    "Menu:\n1. Evaluate Postfix Expression\n2. Convert Infix to Postfix\n3. Exit");

            if (choice == null) {
                // User closed the dialog, exit the program
                running = false;
            } else {
                switch (choice) {
                    case "1":
                        String postfixExpression = JOptionPane.showInputDialog(null, "Enter the postfix expression separated by spaces: ");

                        try {
                            double result = evaluatePostfix(postfixExpression);

                            // Create table model for displaying postfix evaluation steps
                            DefaultTableModel tableModel = new DefaultTableModel();
                            tableModel.addColumn("Expression");
                            tableModel.addColumn("Result");

                            // Add data to table model
                            tableModel.addRow(new Object[]{postfixExpression, result});

                            // Create table and scroll pane
                            DefaultTableModel model = tablePostfixValues(postfixExpression);
                            JTable table = new JTable(model);
                            JScrollPane scrollPane = new JScrollPane(table);

                            // Center align the expression and result columns
                            DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
                            centerRenderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
                            table.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
                            table.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);

                            // Show table in dialog
                            JOptionPane.showMessageDialog(null, scrollPane, "Evaluate Postfix Expression Result", JOptionPane.PLAIN_MESSAGE);
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, "Error: " + e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case "2":
                        String infixExpression = JOptionPane.showInputDialog(null, "Enter an infix expression: ");
                        // Create a table model for displaying infix to postfix conversion steps
                        DefaultTableModel tableModel = convertToPostfixWithSteps(infixExpression);
                        if (tableModel.getRowCount() > 0 && !infixExpression.isEmpty() && validateBrackets(infixExpression) && checkValid(infixExpression)) {
                            // Create table and scroll pane
                            DefaultTableModel model = tableInfixToPostfix(infixExpression);
                            JTable table = new JTable(model);
                            JScrollPane scrollPane = new JScrollPane(table);

                            // Show table in dialog
                            JOptionPane.showMessageDialog(null, scrollPane, "Infix to Postfix Conversion Steps", JOptionPane.PLAIN_MESSAGE);
                        } else {
                            JOptionPane.showMessageDialog(null, "Error: Invalid infix expression", "ERROR", JOptionPane.ERROR_MESSAGE);
                        }
                        break;
                    case "3":
                        running = false;
                        break;
                    default:
                        JOptionPane.showMessageDialog(null, "Invalid choice", "ERROR", JOptionPane.ERROR_MESSAGE);
                        break;
                }
            }
        }
    }
    public static boolean checkValid(String infixExpression) throws StackException {
        MyStack<String> stack = new MyStack<>();
        int num1 =0;
        boolean valid = false;
        if(infixExpression.length() == 1){
            return false;
        }
        String insertString = "";
        for (int i=0 ;i<infixExpression.length();i++){
            Character symbol = infixExpression.charAt(i);
            //If statement to check if a character is a Letter or Digit, or an Operand, else other different characters like '!' '?' returns false
            if( symbol=='(' || symbol==')' || symbol=='{' || symbol=='{' || symbol=='[' || symbol==']'){
            }else if(isOperator(symbol) || Character.isLetterOrDigit(symbol) ){
                if(Character.isLetterOrDigit(symbol)){
                    if((i==(infixExpression.length()-1))){
                        insertString+=symbol;
                        stack.push(insertString);
                        insertString="";
                    }else {
                        Character nextSymbol = infixExpression.charAt((i + 1));
                        if (nextSymbol.equals('+') || nextSymbol.equals('-') || nextSymbol.equals('*') || nextSymbol.equals('/') || nextSymbol.equals('^') || nextSymbol.equals('$')|| nextSymbol.equals(')')) {
                            insertString += symbol;
                            stack.push(insertString);
                            insertString = "";
                        } else {
                            insertString += symbol;
                        }
                    }
                }else if(isOperator(symbol)){
                    if(!stack.isEmpty()) {
                        stack.pop();
                    }else{
                        return false;
                    }
                }
                valid= true;
            }else{
                return false;
            }
        }
        if(stack.size()==1){
            valid=false;
            for(int i=0;i<infixExpression.length();i++){
                Character symbol = infixExpression.charAt(i);
                if(isOperator(symbol)){
                    valid=true;
                }
            }
        }else{
            valid=false;
        }
        return valid;
    }
    public static boolean validateBrackets(String infixExpression) throws StackException {
        MyStack<Character> brackets = new MyStack<>();
        Character currentBracket = null;
        for(int i=0;i<infixExpression.length();i++){
            if(infixExpression.charAt(i)=='(' || infixExpression.charAt(i)=='[' || infixExpression.charAt(i)=='{'){
                brackets.push(infixExpression.charAt(i));
            }
            if(infixExpression.charAt(i)==')' || infixExpression.charAt(i)==']' || infixExpression.charAt(i)=='}'){
                if(brackets.isEmpty()){
                    return false;
                }
                currentBracket=brackets.peek();
                if(currentBracket=='(' && infixExpression.charAt(i)==')'){
                    brackets.pop();
                }
                if(currentBracket=='[' && infixExpression.charAt(i)==']'){
                    brackets.pop();
                }
                if(currentBracket=='{' && infixExpression.charAt(i)=='}'){
                    brackets.pop();
                }
            }
        }
        if(brackets.isEmpty()){
            return true;
        }else {
            return false;
        }
    }
    public static DefaultTableModel tableInfixToPostfix(String infixExpression){
        DefaultTableModel tableModel = new DefaultTableModel();
        tableModel.addColumn("Symbol");
        tableModel.addColumn("Postfix Expression");
        tableModel.addColumn("Operator Stack");
        String postfixExpression = "";
        StringBuilder displayOperatorStack = new StringBuilder();
        Character topSymbol = null;
        MyStack<Character> operatorStack = new MyStack<>();
        int index = 0;
        while(index<infixExpression.length()){
            Character symbol = infixExpression.charAt(index);
            if(!symbol.equals(' ')) {
                if (Character.isLetterOrDigit(symbol)) {
                    postfixExpression += symbol;
                } else {
                    while (!operatorStack.isEmpty() && precedence(operatorStack.peek(), symbol)) {
                        topSymbol = operatorStack.pop();
                        postfixExpression += topSymbol;
                    } // end while
                    if (symbol == ')') {
                        while (!(operatorStack.peek() == '(')) {
                            topSymbol = operatorStack.pop();
                            postfixExpression += topSymbol;
                        }
                        operatorStack.pop();
                    } else if (operatorStack.isEmpty() || symbol != ')') {
                        operatorStack.push(symbol);
                    } else { // pop the open parenthesis and discard it
                        topSymbol = operatorStack.pop();
                    }
                }
                displayOperatorStack.replace(0, displayOperatorStack.length(), String.valueOf(operatorStack.toStringForInfixToPostfix()));
                tableModel.addRow(new Object[]{symbol, postfixExpression,displayOperatorStack.toString()});
            }
             index++;
        }
        while(!operatorStack.isEmpty()){
            Character tempTop = operatorStack.pop();
            postfixExpression += tempTop;
            displayOperatorStack.replace(0,displayOperatorStack.length(), String.valueOf(operatorStack.toStringForInfixToPostfix()));
            tableModel.addRow(new Object[]{"", postfixExpression,displayOperatorStack.toString()});
        }// end while
        String tempPostfixExpression = "";
        for(int i=0;i<postfixExpression.length();i++){
            tempPostfixExpression += postfixExpression.charAt(i)+" ";
        }
        return tableModel;
    }

    // Method to create a table model for displaying postfix evaluation steps
    public static DefaultTableModel tablePostfixValues(String postfixExpression){
        DefaultTableModel tableModel = new DefaultTableModel();
        String calculatedValue = "";
        MyStack<String> operandStack = new MyStack<>();
        StringBuilder displayOperandStack = new StringBuilder();
        tableModel.addColumn("Symbol");
        tableModel.addColumn("Operand 1");
        tableModel.addColumn("Operand 2");
        tableModel.addColumn("Value");
        tableModel.addColumn("Operator Stack");
        String displayOperand1 = "";
        String displayOperand2 = "";
        int index=0;
        while(index<postfixExpression.length()){
            //Token
            Character symbol = postfixExpression.charAt(index);
            if(!symbol.equals(' ')){
                if(Character.isDigit(symbol)){
                    operandStack.push(String.valueOf(symbol));
                    displayOperandStack.replace(0,displayOperandStack.length(), String.valueOf(operandStack));
                }else if(isOperator(symbol)){
                    int operand2 = Integer.parseInt(String.valueOf(operandStack.pop()));
                    int operand1 = Integer.parseInt(String.valueOf(operandStack.pop()));
                    int value = (int) performOperation(symbol,operand1,operand2);
                    displayOperand1 = String.valueOf(operand1);
                    displayOperand2 = String.valueOf(operand2);
                    calculatedValue = String.valueOf(value);
                    operandStack.push(String.valueOf(value));
                    displayOperandStack.replace(0,displayOperandStack.length(), String.valueOf(operandStack));

                }
                tableModel.addRow(new Object[]{symbol,displayOperand1,displayOperand2,calculatedValue,displayOperandStack.toString()});
            }
            index++;
        }
        tableModel.addRow(new Object[]{});
        tableModel.addRow(new Object[]{"Result: "+calculatedValue});
        return tableModel;
    }

    // Method to evaluate a postfix expression and return the result
    public static double evaluatePostfix(String postfixExpression) {
        MyStack<Double> stack = new MyStack<>();

        String[] tokens = postfixExpression.split("\\s+");

        for (String token : tokens) {
            if (isNumeric(token)) {
                stack.push(Double.parseDouble(token));
            } else {
                double operand2 = stack.pop();
                double operand1 = stack.pop();

                double result = performOperation(token.charAt(0), operand1, operand2);
                stack.push(result);
            }
        }

        return stack.pop();
    }


    // Method to perform the specified mathematical operation
    private static double performOperation(char operator, double operand1, double operand2) {
        switch (operator) {
            case '+':
                return operand1 + operand2;
            case '-':
                return operand1 - operand2;
            case '*':
                return operand1 * operand2;
            case '$':
                return Math.pow(operand1,operand2);
            case '/':
                if (operand2 == 0) {
                    throw new ArithmeticException("Division by zero");
                }
                return operand1 / operand2;
            default:
                throw new IllegalArgumentException("Invalid operator: " + operator);
        }
    }

    // Method to create a table model for displaying infix to postfix conversion steps
    public static DefaultTableModel convertToPostfixWithSteps(String infixExpression) {
        // Create table model
        DefaultTableModel tableModel = new DefaultTableModel();
        tableModel.addColumn("Step");
        tableModel.addColumn("Infix Expression");
        tableModel.addColumn("Postfix Expression");

        // Remove spaces from the infix expression
        infixExpression = infixExpression.replaceAll("\\s+", "");

        // Initialize stack and postfix expression string
        MyStack<Character> stack = new MyStack<>();
        StringBuilder postfixExpression = new StringBuilder();

        // Process each character in the infix expression
        for (int i = 0; i < infixExpression.length(); i++) {
            char c = infixExpression.charAt(i);

            if (Character.isLetterOrDigit(c)) {
                // Append operands directly to the postfix expression
                postfixExpression.append(c);
            } else if (c == '(') {
                // Push opening parenthesis to the stack
                stack.push(c);
            } else if (c == ')') {
                // Pop operators from the stack until an opening parenthesis is encountered
                while (!stack.isEmpty() && stack.peek() != '(') {
                    postfixExpression.append(stack.pop());
                }

                if (!stack.isEmpty() && stack.peek() == '(') {
                    stack.pop(); // Discard the opening parenthesis
                }
            } else {
                // Operator encountered

                // Pop operators from the stack and append to the postfix expression
                // until an operator with lower or equal precedence is encountered
                while (!stack.isEmpty() && getPrecedence(c) <= getPrecedence(stack.peek())) {
                    postfixExpression.append(stack.pop());
                }

                // Push the current operator to the stack
                stack.push(c);
            }

            // Add a row to the table model with the current step information
            tableModel.addRow(new Object[]{i + 1, infixExpression.substring(i), postfixExpression.toString()});
        }

        // Pop any remaining operators from the stack and append to the postfix expression
        while (!stack.isEmpty()) {
            if (stack.peek() == '(' || stack.peek() == ')') {
                // Invalid expression with unmatched parentheses
                return new DefaultTableModel();
            }
            postfixExpression.append(stack.pop());
        }

        // Add the final step to the table model with the complete infix and postfix expressions
        tableModel.addRow(new Object[]{infixExpression.length() + 1, "", postfixExpression.toString()});

        return tableModel;
    }

    // Method to get the precedence of an operator
    private static int getPrecedence(char operator) {
        switch (operator) {
            case '+':
            case '-':
                return 1;
            case '*':
            case '/':
                return 2;
            case '$':
                return 3;
            default:
                return -1; // Invalid operator
        }
    }

    // Method to check if a string is numeric
    private static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    // Method to check if a character is a valid operator
    public static boolean isOperator(Character symbol){
        if(symbol == '+' || symbol == '*' || symbol == '-' || symbol == '/' || symbol == '$' || symbol == '^'){
            return true;
        }else{
            return false;
        }
    }
    //precedence method where it returns true if the first operator is higher level than the second operator
    public static boolean precedence(Character operator1, Character operator2){
        //If the first operator is add or subtract, return true if second operator is add or subtract, else all other operator return false
        if(operator1 == '+' || operator1 == '-'){
            if(operator2 == '*' || operator2 == '/' || operator2 == '^'|| operator2 =='(' || operator2=='$'){
                return false;
            }else{
                return true;
            }
            //If the first operator is multiply or divide, return false if the second operator is exponent or parenthesis, else all other operator (add, subtract, divide, multiply) return true
        }else if(operator1 == '*' || operator1 == '/'){
            if(operator2 == '^' || operator2 == '('){
                return false;
            }else{
                return true;
            }
            //If the first operator is an exponent, return false if the second operator is parenthesis, else all other operator return true.
        }else if(operator1=='^' || operator1 == '$'){
            if(operator2=='('){
                return false;
            }else{
                return true;
            }
            //If the first operator is a parenthesis, return false if the second operator is closing parenthesis, else all other operator return true
        } else if(operator1=='('){
            return false;
        }else{
            return true;
        }
    }
}