package IT212_9443_midGroupA.midlab1;

public class Token {
    // Variable declaration
    private char key;
    private String T;

    // Empty constructor
    public Token(){

    }

    // constructor that takes a char as a parameter and sets the key to the char
    public Token(char key){
        this.key = key;
    }
    // Constructor that takes a string as a parameter and sets T to the string
    public Token(String T){
      this.T = T;
    }

    /**
     * Method that returns a string representation of the object
     * @return key value of node
     */
    @Override
    public String toString(){
        return String.valueOf(key);
    }

    /**
     * Method that returns the key of the current node
     * @return key of node
     */
    public char getKey(){
        return key;
    }

    /**
     * Method that returns the value of the variable T
     * @return the value of variable T
     */
    public String getT(){
        return T;
    }
}// end of Token class

