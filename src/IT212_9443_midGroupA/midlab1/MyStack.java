package IT212_9443_midGroupA.midlab1;

public class MyStack<T> implements MyStackInterface<T> {
    // Variable declaration
    private Node<T> top;
    private int count = 0;

    /**
     * insert element into the stack
     * @param item
     * @throws StackException
     */
    @Override
    public void push(T item) throws StackException {
        Node<T> newNode = new Node<>(item,null);
        if (isEmpty()) {
            top = newNode;
        } else {
            newNode.setNext(top);
            top = newNode;
        }
        count += 1;
        return;
    }

    /**
     *  returns and removes last element in the stack
     * @return last element in stack
     * @throws StackException if stack is empty
     */
    @Override
    public T pop() throws StackException {
        T topElement = null;
        if (isEmpty())
            throw new StackException("Stack is empty.");
        else {
            topElement = top.getDatum();
            if (count == 1) {
                top = null;
            }else {
                top = top.getNext();
            }
            count -= 1;
        }
        return topElement;
    }

    /**
     *
     * @return top element of stack
     * @throws StackException
     */
    @Override
    public T peek() throws StackException {
        T topElement = null;
        if(isEmpty())
            throw new StackException("Stack is empty.");
        else {
            topElement = top.getDatum();
        }
        return topElement;
    }

    /**
     * returns number of item/s in the stack
     * @return size
     */
    @Override
    public int size() {
        return count;
    }

    /**
     * Checks if stack is empty
     * @return
     */
    @Override
    public boolean isEmpty() {
        return count == 0;
    }

    /**
     * deletes an element in the stack
     */
    public void delete(){
        while (!isEmpty()){
            top = top.getNext();
        }
    }

    public String toString() {
        Node n = top;
        if(top==null){
            return "";
        }
        StringBuilder output = new StringBuilder();
        while(n.getNext()!=null){
            output.insert(0,","+n.getDatum());
            n=n.getNext();
        }
        output.insert(0,n.getDatum());
        return String.valueOf(output);
    }
    public String toStringForInfixToPostfix(){
        Node n = top;
        if(top==null){
            return "";
        }
        StringBuilder output = new StringBuilder();
        while(n.getNext()!=null){
            output.insert(0,n.getDatum());
            n=n.getNext();
        }
        output.insert(0,n.getDatum());
        return String.valueOf(output);
    }
} //end of MyStack
