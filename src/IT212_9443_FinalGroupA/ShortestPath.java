package IT212_9443_FinalGroupA;
import java.util.*;
public class ShortestPath {
    private Map<String, List<Node>> adjacencyList;


    public ShortestPath() {
        this.adjacencyList = new HashMap<>();
    }


    public void setAdjacencyList(Map<String, List<Node>> adjacencyList) {
        this.adjacencyList = adjacencyList;
    }


    public void addEdge(String source, String destination, int weight) {
        adjacencyList.computeIfAbsent(source, k -> new ArrayList<>()).add(new Node(destination, weight));
    }


    public void dijkstra(String startVertex, String endVertex) {
        int startIndex = getIndex(startVertex);
        int endIndex = getIndex(endVertex);


        if (startIndex == -1 || endIndex == -1) {
            System.out.println("Error: One or both vertices are not found in the graph.");
            return;
        }


        Map<String, Integer> distance = new HashMap<>();
        Map<String, String> previous = new HashMap<>();
        boolean[] visited = new boolean[adjacencyList.size()];
        PriorityQueue<Node> priorityQueue = new PriorityQueue<>(new NodeComparator());


        for (String vertex : adjacencyList.keySet()) {
            distance.put(vertex, Integer.MAX_VALUE);
        }


        distance.put(startVertex, 0);
        priorityQueue.add(new Node(startVertex, 0));


        while (!priorityQueue.isEmpty()) {
            Node currentNode = priorityQueue.poll();
            String currentVertex = currentNode.vertex;


            if (getIndex(currentVertex) == -1) {
                continue;
            }


            if (visited[getIndex(currentVertex)]) {
                continue;
            }


            visited[getIndex(currentVertex)] = true;


            for (Node neighbor : adjacencyList.getOrDefault(currentVertex, Collections.emptyList())) {
                String nextVertex = neighbor.vertex;
                int newDistance = distance.get(currentVertex) + neighbor.weight;


                Integer currentDistance = distance.get(nextVertex);
                if (currentDistance == null || newDistance < currentDistance) {
                    distance.put(nextVertex, newDistance);
                    previous.put(nextVertex, currentVertex);
                    priorityQueue.add(new Node(nextVertex, newDistance));
                }
            }


        }


        List<String> path = new ArrayList<>();
        String currentVertex = endVertex;
        while (currentVertex != null) {
            path.add(currentVertex);
            currentVertex = previous.get(currentVertex);
        }
        Collections.reverse(path);


        System.out.println("The shortest path from vertex " + startVertex +
                " to vertex " + endVertex + " is " + String.join(" -> ", path));


        int dist = distance.getOrDefault(endVertex, -1);
        if (dist != -1 && !(dist == Integer.MAX_VALUE)) {
            System.out.println("The length of the shortest path is " + dist);
            System.out.println();
        } else {
            System.out.println("Error: There is no path from vertex " + startVertex +
                    " to vertex " + endVertex);
            System.out.println();
        }
    }


    private int getIndex(String vertex) {
        int index = 0;
        for (String v : adjacencyList.keySet()) {
            if (v.equalsIgnoreCase(vertex)) {
                return index;
            }
            index++;
        }
        return -1;
    }


    public static class Node implements Comparable<Node> {
        private String vertex;
        private int weight;


        public Node(String vertex, int weight) {
            this.vertex = vertex;
            this.weight = weight;
        }


        @Override
        public int compareTo(Node other) {
            return Integer.compare(weight, other.weight);
        }
    }


    public static class NodeComparator implements Comparator<Node> {
        @Override
        public int compare(Node node1, Node node2) {
            return Integer.compare(node1.weight, node2.weight);
        }
    }
}
