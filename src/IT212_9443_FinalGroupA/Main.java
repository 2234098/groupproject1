package IT212_9443_FinalGroupA;
import java.io.*;
import java.util.*;
public class Main <T>{
    private Graph<T> graph;
    private String[] vertices;
    private boolean loadGraphFile=false;
    public static void main (String[] args){
        try {
            Main main = new Main<>();
            main.run();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void run() {
        Scanner keyboard = new Scanner(System.in);
        ShortestPath shortestPath = new ShortestPath();

        try {
            loadGraphFile(shortestPath);
            int choice;
            do {
                printMenu();
                System.out.print("Please enter your choice: ");
                choice = keyboard.nextInt();
                keyboard.nextLine();

                switch (choice) {
                    case 1:
                        try {
                            listVerticesAndEdges();
                            if(!loadGraphFile){
                                System.out.println("Graph data file successfully loaded");
                                loadGraphFile=true;
                            }else{
                                System.out.println("Error! Graph data is already loaded");
                            }
                            System.out.print("Press enter to continue");
                            keyboard.nextLine();
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                        break;
                    case 2:
                        if(loadGraphFile){
                        performDepthFirstTraversal(keyboard);
                        }else{
                            System.out.println("Error! Please load graph data file first");
                        }
                        System.out.print("Press enter to continue");
                        keyboard.nextLine();
                        keyboard.nextLine();
                        break;
                    case 3:
                        if(loadGraphFile) {
                            performBreadthFirstTraversal(keyboard);
                        }else{
                            System.out.println("Error! Please load graph data file first");
                        }
                        System.out.print("Press enter to continue");
                        keyboard.nextLine();
                        keyboard.nextLine();
                        break;
                    case 4:
                        if(loadGraphFile) {
                            performShortestPath(shortestPath);
                        }else{
                            System.out.println("Error! Please load graph data file first");
                        }
                        System.out.print("Press enter to continue");
                        keyboard.nextLine();
                        break;
                    case 5:
                        System.out.println("Thank you for using the program. Goodbye!");
                        break;
                    default:
                        System.out.println("Invalid choice. Please enter a number between 1 and 5.");
                        System.out.println();
                        break;
                }
            } while (choice != 5);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            keyboard.close();
        }
    }

    private void loadGraphFile(ShortestPath shortestPath) throws FileNotFoundException {
        Scanner fileScanner = new Scanner(new File("graph.txt"));
        vertices = fileScanner.nextLine().split(",");
        graph = new Graph<>(vertices.length);
        Map<String, List<ShortestPath.Node>> adjacencyList = new HashMap<>();

        while (fileScanner.hasNext()) {
            String[] edgeData = fileScanner.nextLine().split(",");
            Character source = edgeData[0].charAt(0);
            char destination = edgeData[1].charAt(0);
            int weight = Integer.parseInt(edgeData[2]);

            adjacencyList.computeIfAbsent(String.valueOf(source), k -> new ArrayList<>()).add(new ShortestPath.Node(String.valueOf(destination), weight));
            adjacencyList.putIfAbsent(String.valueOf(destination), new ArrayList<>());

            int sourceIndex = source - 'A';
            int destIndex = destination - 'A';
            graph.addWeightedEdge(sourceIndex, destIndex, weight);
        }
        shortestPath.setAdjacencyList(adjacencyList); // Set the adjacency list in ShortestPath

        fileScanner.close();
    }

    private void listVerticesAndEdges() throws IOException {
        System.out.println("Vertices: " + Arrays.toString(vertices));
        readEdges();
        System.out.println();
    }
/**
 * Reads and prints edges of the graph from the file.
 *
 * @throws FileNotFoundException If the file "graph.txt" is not found.
 */
private void readEdges() throws FileNotFoundException {
    Scanner fileScanner = new Scanner(new File("graph.txt"));
    fileScanner.nextLine();
    while (fileScanner.hasNext()) {
        String[] edgeData = fileScanner.nextLine().split(",");
        String source = edgeData[0];
        String destination = edgeData[1];
        int weight = Integer.parseInt(edgeData[2]);
        System.out.println("Edge: " + source + " -> " + destination + ", Weight: " + weight);
    }
    fileScanner.close();
}


    /**
     * Performs a depth-first traversal on a graph and prints the result.
     */
    private void performDepthFirstTraversal(Scanner keyboard) {
        System.out.println("Given Vertices : "+Arrays.toString(vertices));
        // Use char for vertex input
        System.out.print("Please enter the starting vertex: ");
        char startVertex = keyboard.next().toUpperCase().charAt(0);
        int startVertexIndex = startVertex - 'A';

        graph.depthFirstTraversal(startVertexIndex);
        System.out.println();
    }

    /**
     * Performs a breadth-first traversal on a graph and prints the result.
     */
    private void performBreadthFirstTraversal(Scanner keyboard) {
        System.out.println("Given Vertices : "+Arrays.toString(vertices));
        // Use char for vertex input
        System.out.print("Please enter the starting vertex: ");
        char startVertex = keyboard.next().toUpperCase().charAt(0);
        int startVertexIndex = startVertex - 'A';
        graph.breadthFirstTraversal(startVertexIndex);
    }

    /**
     * Performs Dijkstra's algorithm to find the shortest path between two vertices.
     *
     * @param shortestPath The ShortestPath object to perform the algorithm.
     */
    private void performShortestPath(ShortestPath shortestPath) {
        System.out.println("Given Vertices : "+Arrays.toString(vertices));
        Scanner kbd = new Scanner(System.in);
        System.out.print("Please enter the starting vertex: ");
        String startVertex = kbd.nextLine().toUpperCase();
        System.out.print("Please enter the ending vertex: ");
        String endVertex = kbd.nextLine().toUpperCase();
        shortestPath.dijkstra(startVertex, endVertex);
    }
    /**
     * Prints the main menu options for the user.
     */
    private void printMenu() {
        System.out.println("====================================================");
        System.out.println("                        Menu");
        System.out.println("====================================================");
        System.out.println("1. Load file containing the graph's data");
        System.out.println("2. Perform Depth First Traversal of the graph");
        System.out.println("3. Perform Breadth First Traversal of the graph");
        System.out.println("4. Show the shortest path from one vertex to another");
        System.out.println("5. Exit");
        System.out.println("====================================================");
    }
}


