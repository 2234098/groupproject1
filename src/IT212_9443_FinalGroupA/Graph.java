package IT212_9443_FinalGroupA;
import java.util.*;

public class Graph<T> {
    private int vertices;
    private VertexNode<T>[] nodes;
    private static final int INFINITY = Integer.MAX_VALUE;

    /**
     * Initializes a new graph with the specified number of vertices.
     *
     * @param vertices The number of vertices in the graph.
     */

    public Graph(int vertices) {
        this.vertices = vertices;
        nodes = new VertexNode[vertices];
        for (int i = 0; i < vertices; i++) {
            nodes[i] = new VertexNode<>(i);
        }
    }

    /**
     * Gets the count of the vertices in the graph.
     *
     * @return The count of vertices.
     */

    public int getVerticesCount() {
        return vertices;
    }

    /**
     * Gets the count of vertices in the graph.
     *
     * @return The count of vertices.
     */

    public int getVertices() {
        return vertices;
    }

    /**
     * Gets the array of vertex nodes in the graph.
     *
     * @return The array of vertex nodes.
     */

    public VertexNode<T>[] getNodes() {
        return nodes;
    }

    /**
     * Adds a weighted edge between two vertices in the graph.
     *
     * @param source      The source vertex index.
     * @param destination The destination vertex index.
     * @param weight      The weight of the edge.
     */

    public void addWeightedEdge(int source, int destination, int weight) {
        nodes[source].addNeighbor(nodes[destination], weight);
        // Sort the neighbors based on their values
        nodes[source].getNeighbors().sort(Comparator.comparingInt(node -> node.getDestination().getValue()));
    }

    /**
     * Adds an unweighted edge between two vertices in the graph.
     *
     * @param source      The source vertex index.
     * @param destination The destination vertex index.
     */

    public void addEdge(int source, int destination) {
        nodes[source].addNeighbor(nodes[destination], 0);
    }

    /**
     * Performs breadth-first traversal starting from the specified vertex.
     *
     * @param startVertex The starting vertex index.
     */

    public void breadthFirstTraversal(int startVertex){
        System.out.println("Breadth First Traversal: ");
        boolean[] visited = new boolean[vertices];
        Queue<Integer> queue = new LinkedList<>();
        queue.add(startVertex);
        visited[startVertex] = true;

        while(!queue.isEmpty()) {
            int current = queue.poll();
            //System.out.print(current + " ");
            System.out.print((char) ('A' + current) + " "); // Convert index to letter

            for (Edge<T> edge: nodes[current].getNeighbors()) {
                int neighbor = edge.getDestination().getValue() ;
                if (!visited[neighbor]) {
                    visited[neighbor] = true;
                    queue.add(neighbor);
                }
            }
        }
        System.out.println();
    }

    /**
     * Performs depth-first traversal starting from the specified vertex.
     *
     * @param startVertex The starting vertex index.
     */

    public void depthFirstTraversal(int startVertex) {
        System.out.println("Depth First Traversal:");
        boolean[] visited = new boolean[vertices];
        Stack<Integer> stack = new Stack<>();
        stack.push(startVertex);

        while (!stack.isEmpty()) {
            int current = stack.pop();
            if (!visited[current]) {
                //System.out.print(current + " ");
                System.out.print((char) ('A' + current) + " "); // Convert index to letter
                visited[current] = true;
            }

            for (Edge<T> edge : nodes[current].getNeighbors()) {
                int neighbor = edge.getDestination().getValue();
                if (!visited[neighbor]) {
                    stack.push(neighbor);
                }
            }
        }
        System.out.println();
    }

    /**
     * Represents a vertex node in the graph.
     */

    private static class VertexNode<T> {
        private int value;
        private List<Edge<T>> neighbors;

        public VertexNode(int value) {
            this.value = value;
            neighbors = new ArrayList<>();
        }

        public void addNeighbor(VertexNode<T> destination, int weight) {
            neighbors.add(new Edge<>(destination, weight));
        }
        public int getValue() {
            return value;
        }

        public List<Edge<T>> getNeighbors() {
            return neighbors;
        }

    }
    /**
     * Represents an edge between two vertices in the graph.
     */
    private static class Edge<T> {
        private VertexNode<T> destination;
        private int weight;

        public Edge(VertexNode<T> destination, int weight) {
            this.destination = destination;
            this.weight = weight;
        }

        public VertexNode<T> getDestination() {
            return destination;
        }
    }
}


